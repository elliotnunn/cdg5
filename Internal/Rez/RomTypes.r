/*
	File:		ROMTypes.r

	Contains:	The resource type declartions that are used to describe
				the layout of a ROM for the RomLayout tool.
				
				These types are duplicated in RomTypes.h

					rcod -	Rom Code		Where to place code resources.
					rdcl -	DeclData		Where to place the declaration ROM(s).
					rrhd -	Resource Header	Where to locate the ROM resource header.
					rrsc -	Resource		What rom resources to include and
											where to find them.
					rtim -	Time Stamp		Where to add a time stamp.
					rref -	Reference		Where to fixup a reference.
					rcks -	Check Sum		Where to add a check sum.
					rsiz -	Size			What size is the rom image.

	Written by:	Kurt Clark.

	Copyright:	� 1992-1994 by Apple Computer, Inc., all rights reserved.

	Change History (most recent first):

				  1/8/17	HQX		Add prcl type
                12/26/16    HQX     Add interim HIRM type
	   <SM5>	  2/6/94	kc		Added 'rraa' type for absolute address rom resources.
	   <SM4>	10/26/93	kc		Add rrmp for Risc Rom Map
	   <SM3>	 3/31/93	kc		Add rmap resource type to allow the user to tell the RomLayout
									tool to print ROM maps for various resoruces. Update some of the
									other formats to work with the new RomLayout tool.
	   <SM2>	11/23/92	kc		Add RomBase.
*/

#ifndef  __RomTypes__
#define __RomTypes__

#define kFormatVersion	0x01
#define kComboSize 		0x08
#define kkResourceAttr	0x58

/* These are for 'prcl' resources: */

#define kPropertiesForSpecialNode				'node'
#define kPropertiesForExistingNode				'prop'

#define kUseResName								"\000~"


/*
	The following macro allows us to specify filenames using 
	the format:

		{ObjDir},	<--	Shell variable for pathname.
		"Rom.Rsrc"	<-- File name.
	
*/

#define ObjDir $$shell("ObjDir")

#define RsrcDir $$shell("RsrcDir")

#define ImageDir $$shell("ImageDir")

#define NewWorldDir $$shell("NewWorldDir")

#define MiscDir $$shell("MiscDir")

#define TidbitsDir $$shell("TidbitsDir")

#define TextDir $$shell("TextDir")

#define filename 	begin:								\
						array [1] {						\
							string;						\
						};								\
						cstring;						\
					end:								\
					fill bit [ $800 - (end - begin) ];

#define filename2 	begin2:								\
						array [1] {						\
							string;						\
						};								\
						cstring;						\
					end2:								\
					fill bit [ $800 - (end2 - begin2) ];

#define filename3 	begin3:								\
						array [1] {						\
							string;						\
						};								\
						cstring;						\
					end3:								\
					fill bit [ $800 - (end3 - begin3) ];

#define thirtytwo1	begin4:								\
						string;							\
					end4:								\
					fill bit [ $100 - (end4 - begin4) ];

#define thirtytwo2	begin5:								\
						string;							\
					end5:								\
					fill bit [ $100 - (end5 - begin5) ];


/*
	Rom Code Block
*/
type 'rcod' {
	
	longint = kFormatVersion;
	
	filename;						/* Resource file name */
	
	literal longint;				/* Resource type */
	
	integer;						/* Resource id */

	longint kDynamicAddress = -1;	/* Address */
};


/*
	Declaration Rom
*/
type 'rdcl' {
	
	longint = kFormatVersion;
		
	filename;						/* Data filename */

	longint kDynamicAddress = -1;	/* Address */
};


/*
	Rom Resource Header
*/
type 'rrhd' {
	
	longint = kFormatVersion;

	longint kDynamicAddress = -1;	/* Address */

	longint;						/* Fixup Address */
};


/*
	Rom Resource
*/
type 'rrsc' {
	
	longint = kFormatVersion;

	longint;							/* Link time conditional */
	
	byte;								/* Combo bit field (first byte) */
	
	fill byte[ kComboSize - 1 ];		/* Unused combo bytes */
	
	filename;							/* Name of file containing resource */
	
	longint = $$CountOf(RsrcList);		/* Number of entries */
	
	array RsrcList {					/* Entries for ROM resources */

		literal longint;				/* Resource type */
	
		longint = $$CountOf(IDList);	/* Count of IDs */
		
		array IDList {					/* Id's of this type */
		
			integer;					/* Resource id */
		};
	};
};


/*
	Rom Resource Absolute Address
*/
type 'rraa' {
	
	longint = kFormatVersion;

	longint;							/* Link time conditional */
	
	byte;								/* Combo bit field (first byte) */
	
	fill byte[ kComboSize - 1 ];		/* Unused combo bytes */
	
	filename;							/* Name of file containing resource */
	
	literal longint;					/* Resource type */
		
	integer;							/* Resource id */

	longint kDynamicAddress;			/* Resource address */
};


/*
	Rom Time Stamp
*/
type 'rtim' {
	
	longint = kFormatVersion;

	literal longint;				/* Fixup Resource type */
	
	integer;						/* Fixup Resource id */

	longint kDynamicAddress;		/* Fixup address */
};


/*
	Rom Reference
*/
type 'rref' {
	
	longint = kFormatVersion;
	
	literal longint;				/* Referenced Resource type */
	
	integer;						/* Referenced Resource id */

	longint;						/* Referenced address */

	literal longint;				/* Fixup Resource type */
	
	integer;						/* Fixup Resource id */

	longint;						/* Fixup address */
};


/*
	Rom Check Sum
*/
type 'rcks' {
	
	longint = kFormatVersion;

	longint;						/* Starting address */
	
	longint;						/* Ending address */
	
	literal longint;				/* Fixup Resource type */
	
	integer;						/* Fixup Resource id */

	longint;						/* Slice address */

	longint;						/* Checksum address */
};


/*
	Rom Size
*/
type 'rsiz' {
	
	longint = kFormatVersion;

	longint;						/* Size */
};


/*
	Rom Base
*/
type 'rbas' {
	
	longint = kFormatVersion;

	longint;						/* Base */
};


/*
	Rom Map
*/
type 'rmap' {
	
	longint = kFormatVersion;
	
	literal longint;				/* Resource type */
	
	integer;						/* Resource id */

	filename2;						/* Name of file containing the link map */

	filename3;						/* Name of file containing the symbol table */
};


/*
	Risc Rom Map
*/
type 'rrmp' {
	
	longint = kFormatVersion;
	
	literal longint;				/* Resource type */
	
	integer;						/* Resource id */

	filename2;						/* Name of file containing the link map */
};






/*
	Toolbox parcel structure
*/
type 'prcl' {
	longint;						/* parcel type */
	
	namesStart:
	array names {
		nameStart:
		string;
		nameEnd:
		fill bit [ $100 - (nameEnd[$$ArrayIndex(names)] - nameStart[$$ArrayIndex(names)]) ];
	};
	namesEnd:
	fill bit [ $200 - (namesEnd - namesStart) ];
	
	longint;						/* flags */
	
	longint = $$CountOf(ItemList);
	
	Array ItemList {
		
		sBegin:						/* rather awkwardly, this string names the item */
			string;
		sEnd:
		fill bit [ 32*8 - (sEnd[$$ArrayIndex(ItemList)] - sBegin[$$ArrayIndex(ItemList)]) ];
		
		longint; /* flags */
		
		itemBegin:
		
		switch {
		case fromDataFork:
			key longint = 'data';
			
			/* a Str255 made of two string decs: */
			byte = (psend[$$ArrayIndex(ItemList)] - psbegin[$$ArrayIndex(ItemList)]) / 8;
			psbegin:
			string;							/* first path comp */
			string;							/* second path comp */
			psend:
			fill bit [ 255*8 - (psend[$$ArrayIndex(ItemList)] - psbegin[$$ArrayIndex(ItemList)]) ];
		
		case fromResource:
			key longint = 'rsrc';
			
			/* another Str255 made of two string decs: */
			byte = (psend2[$$ArrayIndex(ItemList)] - psbegin2[$$ArrayIndex(ItemList)]) / 8;
			psbegin2:
			string;							/* first path comp */
			string;							/* second path comp */
			psend2:
			fill bit [ 255*8 - (psend2[$$ArrayIndex(ItemList)] - psbegin2[$$ArrayIndex(ItemList)]) ];
			
			literal longint;				/* rsrc type */
			
			/* another Str255, but with only one dec */
			psbegin3:
			pstring;						/* rsrc name */
			psend3:
			fill bit [ 255*8 - (psend3[$$ArrayIndex(ItemList)] - psbegin3[$$ArrayIndex(ItemList)]) ];
		
		case useCstring:
			key longint = 'cstr';
			longint = (csend[$$ArrayIndex(ItemList)] - csbegin[$$ArrayIndex(ItemList)]) / 8;
			csbegin:
			cstring;
			csend:
		
		case useCstringArray:
			key longint = 'csta';
			longint = (csend2[$$ArrayIndex(ItemList)] - csbegin2[$$ArrayIndex(ItemList)]) / 8;
			csbegin2:
			array {
				cstring;
			};
			csend2:
		};
		
		itemEnd:
		fill bit [ 520*8 - (itemEnd[$$ArrayIndex(ItemList)] - itemBegin[$$ArrayIndex(ItemList)]) ];
	};
};

#endif __RomTypes__
