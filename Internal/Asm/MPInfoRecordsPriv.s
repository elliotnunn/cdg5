;	Opaque NanoKernel structures:
;		- stored in the NanoKernel pool (not always)
;		- associated with a class number and opaque ID (not always)
;		- referenced by opaque ID (for MPLibrary's benefit)





;_______________________________________________________________________
;	ID CLASS 1: PROCESS
;	(size: 32b, thud command: id -p)
;
;	The NanoKernel's internal representation of a cooperative process
;	within the blue environment. Processes and tasks have a many-to-one
;	relationship.
;
;	There is a special 'blue' process that owns the blue task and all
;	the CPU idle tasks.
;_______________________________________________________________________

Process					record	0,INCR

kIDClass				equ		1
kSignature				equ		'PROC'

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 00
Signature				ds.l	1	; 04 ; 'PROC'
Unused					ds.l	1	; 08 ; MPCall_5 does something here
SystemAddressSpaceID	ds.l	1	; 0c ; set by Init.s after addrspc creation
TaskCount				ds.l	1	; 10 ; incremented by CreateTask
SystemAddressSpacePtr	ds.l	1	; 14
AddressSpaceCount		ds.l	1	; 18 ; incremented by NKCreateAddressSpaceSub
						ds.l	1	; 1c

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 2: TASK
;	(size: 1k, thud command: id -t)
;
;	What the MPLibrary and NanoKernel call the unit of multitasking.
;	(Remember that 'Thread' and 'Process' were taken.)
;
;	Contains space for a ContextBlock in the style of the ECB, but
;	blue's ContextBlockPtr is redirected to the ECB.
;_______________________________________________________________________

Task					record	0,INCR

kIDClass				equ		2
kSignature				equ		'TASK'

;_______________________
;	Task priorities
;_______________________

kCriticalPriority		equ		0
kLatencyProtectPriority	equ		1
kNominalPriority		equ		2
kIdlePriority			equ		3

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 000
Signature				ds.l	1	; 004
QueueMember				ds.l	4	; 008:018 ; a task is always a member of a queue, e.g. the RDYQ
MysteryByte1			ds.b	1	; 018 ; CreateTask sets 0 by default (blue = 2)
Priority				ds.b	1	; 019 ; CreateTask sets 2 by default
MysteryHalf				ds.w	1	; 01a
Weight					ds.l	1	; 01c ; default is 100, blue gets 200, idle gets 1
						ds.l	1	; 020
						ds.l	1	; 024
						ds.l	1	; 028
						ds.l	1	; 02c
						ds.l	1	; 030
						ds.l	1	; 034
						ds.l	1	; 038
						ds.l	1	; 03c
						ds.l	1	; 040
						ds.l	1	; 044
						ds.l	1	; 048
						ds.l	1	; 04c
						ds.l	1	; 050
						ds.l	1	; 054
						ds.l	1	; 058
						ds.l	1	; 05c
ProcessID				ds.l	1	; 060
ThingThatAlignVecHits	ds.l	1	; 064 ; IntAlignment is interested in bit 9, MPCall 116 in bit 15
						ds.l	1	; 068
OwningProcessPtr		ds.l	1	; 06c
AddressSpacePtr			ds.l	1	; 070 ; borrowed from PROC argument to CreateTask
Name					ds.l	1	; 074 ; 'blue', creator of owning cooperative process, etc
CpuID					ds.l	1	; 078
						ds.l	1	; 07c
CreateTime3				ds.d	1	; 080
ContextBlockPtr			ds.l	1	; 088 ; points internally by default, and to EDP.ECB in blue
VectorSaveArea			ds.l	1	; 08c
						ds.l	1	; 090
						ds.l	1	; 094
						ds.l	1	; 098
NotificationPtr			ds.l	1	; 09c
SemaphoreLLL			ds.l	4	; 0a0:0b0
Zero					ds.l	1	; 0b0
One						ds.l	1	; 0b4
						ds.l	1	; 0b8
						ds.l	1	; 0bc
Zero1					ds.l	1	; 0c0
Zero2					ds.l	1	; 0c4
CreateTime1				ds.d	1	; 0c8
CreateTime2				ds.d	1	; 0d0
						ds.l	1	; 0d8
						ds.l	1	; 0dc
Zero3					ds.l	1	; 0e0
Zero4					ds.l	1	; 0e4
Zero5					ds.l	1	; 0e8
						ds.l	1	; 0ec
YellowVecTblPtr			ds.l	1	; 0f0
						ds.l	1	; 0f4
						ds.l	1	; 0f8
						ds.l	1	; 0fc
ContextBlock			ds.b	768	; 100:400 ; like the EDP's Emulator Context Block -- unsure of size

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 3: TIMER
;	(size: 64b, thud command: id -tm)
;
;_______________________________________________________________________

Timer					record	0,INCR

kIDClass				equ		3
kSignature				equ		'TIME'

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 00
Signature				ds.l	1	; 04
						ds.l	1	; 08
						ds.l	1	; 0c
ProcessID				ds.l	1	; 10
						ds.l	1	; 14
						ds.l	1	; 18
						ds.l	1	; 1c
						ds.l	1	; 20
						ds.l	1	; 24
						ds.l	1	; 28
						ds.l	1	; 2c
						ds.l	1	; 30
						ds.l	1	; 34
						ds.l	1	; 38
						ds.l	1	; 3c

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 4: QUEUE
;	(size: 52b, thud command: id -q)
;
;_______________________________________________________________________

Queue					record	0,INCR

kIDClass				equ		4

;_______________________
;	Fields
;_______________________

LLL						ds.l	4	; 00:10
						ds.l	4	; 10:20
ProcessID				ds.l	1	; 20
						ds.l	1	; 24
						ds.l	1	; 28
						ds.l	1	; 2c
						ds.l	1	; 30

Size					equ		*
						endr



ReadyQueue				record	0,INCR

LLL						ds.l	4	; 00:10 ; nothing fancy
Counter					ds.l	1	; 10 ; InitRDYQs sets, TaskReadyAsNext bumps
TotalWeight				ds.l	1	; 14 ; divide available time by these
Timecake				ds.d	1	; 18 ; period of ~1ms, 8ms, 64ms, 512ms


						org		0x20
;	Constants
kSignature				equ		'RDYQ'

						endr




kTimerQueueSignature	equ		'TMRQ'
kDelayQueueSignature	equ		'DLYQ'
kDbugQueueSignature		equ		'DBUG'
kPageQueueSignature		equ		'PAGQ'
kNotQueueSignature		equ		'NOTQ'
kSemaQueueSignature		equ		'SEMQ'




;_______________________________________________________________________
;	ID CLASS 5: SEMAPHORE
;	(size: 32b, thud command: id -s)
;
;_______________________________________________________________________

Semaphore				record	0,INCR

kIDClass				equ		5
kSignature				equ		'SEMA'

;_______________________
;	Fields
;_______________________

LLL						ds.l	4	; 00:10
						ds.l	1	; 10
						ds.l	1	; 14
ProcessID				ds.l	1	; 18
						ds.l	1	; 1c

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 6: CRITICAL REGION
;	(size: 36b, thud command: id -r)
;
;_______________________________________________________________________

CriticalRegion			record	0,INCR

kIDClass				equ		6
kSignature				equ		'CRGN'

;_______________________
;	Fields
;_______________________

LLL						ds.l	4	; 00:10
ProcessID				ds.l	4	; 10:20 ; lll.freeform is the field?
						ds.l	1	; 20

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 7: CPU
;	(size: 32+800+128 = 960b, thud command: id -c)
;
;_______________________________________________________________________

CPU						record	0,INCR

kIDClass				equ		7
kSignature				equ		'CPU '

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 00
Signature				ds.l	1	; 04
CgrpList				ds.l	4	; 08:18
Eff						ds.l	1	; 18 ; contains 0x0000000f
IdleTaskPtr				ds.l	1	; 1c

EWABase					ds.b	800	; negative-indexed parts of EWA
EWA						ds.b	128	; positive-indexed parts of EWA

Size					equ		*
						endr





;_______________________________________________________________________
;	ID CLASS 8: ADDRESS SPACE
;	(size: 192b, thud command: id -sp)
;
;_______________________________________________________________________

AddressSpace 			record	0,INCR

kIDClass				equ		8
kSignature				equ		'SPAC'

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 00
Signature				ds.l	1	; 04
						ds.l	1	; 08
TaskCount				ds.l	1	; 0c ; incremented by CreateTask
RsrvList				ds.l	4	; 10:20 ; LLL
AreaList				ds.l	4	; 20:30 ; LLL
SRs						ds.l	16	; 30:70 ; segment register values
ParentCoherenceSpecialPtr ds.l	1	; 70 ; SpecialPtr of owning cgrp (in list owned by Cpu)
ProcessID				ds.l	1	; 74 ; ID of owning PROC
						ds.l	1	; 78
						ds.l	1	; 7c
BATs
BAT0U					ds.l	1	; 80
BAT0L					ds.l	1	; 84
BAT1U					ds.l	1	; 88
BAT1L					ds.l	1	; 8c
BAT2U					ds.l	1	; 90
BAT2L					ds.l	1	; 94
BAT3U					ds.l	1	; 98
BAT3L					ds.l	1	; 9c
ExtraBATs							; the flag that enables these is never set?
ExtraBAT0U				ds.l	1	; a0
ExtraBAT0L				ds.l	1	; a4
ExtraBAT1U				ds.l	1	; a8
ExtraBAT1L				ds.l	1	; ac
ExtraBAT2U				ds.l	1	; b0
ExtraBAT2L				ds.l	1	; b4
ExtraBAT3U				ds.l	1	; b8
ExtraBAT3L				ds.l	1	; bc

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 9: EVENT GROUP
;	(size: 32b, thud command: id -e)
;
;_______________________________________________________________________

EventGroup				record	0,INCR

kIDClass				equ		9
kSignature				equ		'EVNT'

;_______________________
;	Fields
;_______________________

LLL						ds.l	4	; 00:10 ; first field is ID
						ds.l	1	; 10
ProcessID				ds.l	1	; 14
						ds.l	1	; 18 ; contains 1-8 +/- 16
						ds.l	1	; 1c

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 10: COHERENCE GROUP
;	(size: 88b, thud command: id -cg)
;
;_______________________________________________________________________

CoherenceGroup			record	0,INCR

kIDClass				equ		10
kSignature				equ		'CGRP'

;_______________________
;	Fields
;_______________________

LLL						ds.l	4	; 00:10 ; element in CpuStructs list of cgrps
GRPSList				ds.l	4	; 10:20 ; my own list of GRPSes (what are they?)
CpuCount				ds.l	1	; 20
ScheduledCpuCount		ds.l	1	; 24
						ds.l	1	; 28
						ds.l	1	; 2c
						ds.l	1	; 30
						ds.l	1	; 34
						ds.l	1	; 38
						ds.l	1	; 3c
						ds.l	1	; 40
						ds.l	1	; 44
Incrementer				ds.l	1	; 48 ; number of NKCreateAddressSpaceSub calls % 1M
						ds.l	1	; 4c
						ds.l	1	; 50
						ds.l	1	; 54

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 11: AREA
;	(size: 160b, thud command: id -a)
;
;	A contiguous region of effective addresses with similar properties.
;_______________________________________________________________________

Area					record	0,INCR

kIDClass				equ		11
kSignature				equ		'AREA'

;_______________________
;	Fields
;_______________________

ID						ds.l	1	; 00
Signature				ds.l	1	; 04
TwoFiftySix				ds.l	1	; 08
ProcessID				ds.l	1	; 0c
AddressSpaceID			ds.l	1	; 10
						ds.l	1	; 14
						ds.l	1	; 18
						ds.l	1	; 1c
						ds.l	1	; 20
LogicalBase				ds.l	1	; 24
LogicalBase2			ds.l	1	; 28
Length					ds.l	1	; 2c ; in actual bytes!
Zero					ds.l	1	; 30
						ds.l	1	; 34
Length2					ds.l	1	; 38
						ds.l	1	; 3c
						ds.l	1	; 40
						ds.l	1	; 44
						ds.l	1	; 48
						ds.l	1	; 4c
						ds.l	1	; 50
LLL						ds.l	4	; 54:64 ; member of address space
						ds.l	1	; 64
						ds.l	1	; 68
AddressSpacePtr			ds.l	1	; 6c
						ds.l	1	; 70
						ds.l	1	; 74
						ds.l	1	; 78
						ds.l	1	; 7c
						ds.l	1	; 80
						ds.l	1	; 84
						ds.l	1	; 88
						ds.l	1	; 8c
						ds.l	1	; 90
						ds.l	1	; 94
						ds.l	1	; 98
						ds.l	1	; 9c

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 12: NOTIFICATION
;	(size: 40b, thud command: id -n)
;
;_______________________________________________________________________

Notification			record	0,INCR

kIDClass				equ		12
kSignature				equ		'KNOT'

;_______________________
;	Fields
;_______________________

						ds.l	1	; 00
Signature				ds.l	1	; 04
ProcessID				ds.l	1	; 08
						ds.l	1	; 0c
						ds.l	1	; 10
						ds.l	1	; 14
						ds.l	1	; 18
						ds.l	1	; 1c
						ds.l	1	; 20
						ds.l	1	; 24

Size					equ		*
						endr




;_______________________________________________________________________
;	ID CLASS 13: CONSOLE LOG
;	(size: 16b, thud command: id -nc)
;
;	Never seen one in the wild -- must have been in debug builds.
;_______________________________________________________________________

ConsoleLog				record	0,INCR

kIDClass				equ		13

;_______________________
;	Fields
;_______________________

						ds.l	1	; 00
						ds.l	1	; 04
ProcessID				ds.l	1	; 08
						ds.l	1	; 0c

Size					equ		*
						endr
