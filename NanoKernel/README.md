# Builtin initialiation #

Lives in ROM, gets control from Trampoline, blah blah.

# Replacement kernel initialisation #

Lives in System file, gets control from boot 3, blah blah.

# Structures #

# Interfaces #

# Interrupt handling #

## PowerPC vector table ##

When a processor takes an interrupt, it begins executing read-only code in the PowerPC interrupt vector table (in ROM, of course). To begin to make sense of this interrupt, the vector code must consult the SPRGs, four privileged special purpose registers.

*Most* of the PowerPC vectors in `:RISC:ExceptionTable.s` do the mininum required to hand control to the NanoKernel:
- Save r1 in SPRG1.
- Save LR in SPRG2.
- Load physical address of kernel code from SPRG3+n.
- blrl to that address.

### Vector tables ###

The IVT is actually only interested in SPRG3 ('vecBase'), into which the NanoKernel places a pointer to one of its own structures. This structure, a 48-membered table of physical pointers into the NanoKernel code, is described as `VecTable` in `:NanoKernel:NKInfoRecordsPriv.s`. Each IVT handler in ROM makes an indirect jump via its (roughly) corresponding entry in the table. For example: `sc` => syscall interrupt => code at IVT+0xC00 => jumps to *(SPRG3+0x30), which is probably `kcMPDispatch` in `:NanoKernel:kcMP.s`. Each processor naturally has its own SPRG3, so at any time it can be in one of several "modes" for interrupt handling. The only vector tables that I know (except perhaps the `Indigo` ones) are populated at init time and live in the `KDP` or private page. Knowing little about these tables, I have given them colour codes (`YellowVecBase` etc.). Yellow seems to be the table corresponding most closely to "normal operation".

### A few register conventions ###


That address is usually a function in `Interrupts.s` titled `IntSomething`. And its first instruction is usually:
```
bl int_prepare
```

The function int_prepare does this:
- Save r1 (via SPRG1) and r6 to this CPU's EWA.
- Save r0 and r7-r13 to the current task's ContextBlock.
- Load: 0->r0, KDP->r1, CB->r6, Flags->r7, EWA->r8, SRR0/1->r10/r11, LR->r12, CR->r13
- Leave r2-r5, r9 and r14-r31 be. (Of these, only r9 is saved.)

And then `IntSomething` might also call `Save_r14_r31` to get more scratch space. This function assumes that r6 contains a CB ptr and saves the registers there. It clobbers r8 but then restores it from SPRG0 (EWA pointer), so stop worrying.








#### Conventions at SUP time ####

r1 = kdp (or maybe private ewa)
r3 = 68k A0 (at least for RESET trap)
r4 = 68k A1 (same)
r5 = 
r10 = 
r11 = 
ContextBlock.r7 = 
r6 = ECB

`Skeleton key inserted at (r11 & ~r5 | r7) (r10)`

"This instruction is used to perform a programmed reset of all peripherals connected to the 68000's RESET* pin."