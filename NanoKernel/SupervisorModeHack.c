/**************************** Code for 68K application ***********************/
struct PPCRegisterList
{
        unsigned long   PC;
        unsigned long   GPR[32];
        double          FPR[32];
};
typedef struct PPCRegisterList PPCRegisterList;

inline asm unsigned long _GetA7(void) {move.l a7,d0; rts;}
void EnterPriviledgedMode(PPCRegisterList* regList);

void main(void)
{
        CFragConnectionID               connID;
        Str255                          errName;
        unsigned long*                  mainAddr;
        GDHandle                                dev;
        PPCRegisterList         regList;
       
        // We hide the cursor because on my PowerMac 7500/120(604) I get a wierd vertical line at the position of
        // the mouse cursor when I try to draw to the screen in priviledged mode..  Calling HideCursor() seems to remove
        // that problem.
        HideCursor();
       
        // Load the Super Mode Code shared library.  This library exports its main() routine.  That is the routine
        // that is going to get called in priviledged mode.
        if(GetSharedLibrary("\pSuper Mode Code",kPowerPCArch,kLoadLib,&connID,(Ptr*)&mainAddr,errName))
                ExitToShell();  // Failed to load the Super Mode Code shared library.
       
        // Set up some the registers that we will be passing information in.  PowerPC calling conventions put the
        // arguments in registers r3,r4,etc..  So we load our arguments starting in r3.  Also, we set up the
        // R1 (stack pointer) register to use the current stack.   Finally, we use the PPC transition vector returned
        // by GetSharedLibrary as the starting point for our priviledged mode code.  mainAddr[0] is the address
        // of the routine and mainAddr[1] is the RTOC of the routine.
        dev = GetMainDevice();
        regList.PC = mainAddr[0];                                                                                              
// Address of the main() routine
        regList.GPR[1] = _GetA7();                                                                                            
// The stack for our stuff
        regList.GPR[2] = mainAddr[1];                                                                                          
// RTOC of the Super Mode Code main() routine
        regList.GPR[3] = (unsigned long)(**(**dev).gdPMap).baseAddr;                                    // The base address of the screen
        regList.GPR[4] = (**(**dev).gdPMap).rowBytes & 0x3FFF;                                          // The number of bytes in each row on the screen
        regList.GPR[5] = (**(**dev).gdPMap).bounds.bottom - (**(**dev).gdPMap).bounds.top;      // The height in pixels of the screen
       
        // The EnterPriviledgedMode routine executes the 68K reset instruction after putting the register information
        // in the right places.
        EnterPriviledgedMode(&regList);
}

asm void EnterPriviledgedMode(PPCRegisterList* regList)
{
        // Disable 68K interrupts
        move    #0x2700,sr;
       
        // Load the address of the register list into a3
        move.l  0x0004(sp),a3;
       
        // Mess with the stack!
        move.l  sp,d0;
        andi.w  #0xFC00,d0;
        move.l  d0,sp;
        move.w  #0x00BF,d0;
@loop:
        clr.l           -(sp);
        dbra            d0,@loop;
       
        // Set up the program counter
        move.l struct(PPCRegisterList.PC)(a3),0x00FC(sp);                               // PC
       
        // Set up the general-purpose registers
        move.l  struct(PPCRegisterList.GPR)+0*4(a3),0x0100+0*8+4(sp);  // R0
        move.l  struct(PPCRegisterList.GPR)+1*4(a3),0x0100+1*8+4(sp);  // R1
        move.l  struct(PPCRegisterList.GPR)+2*4(a3),0x0100+2*8+4(sp);  // R2
        move.l  struct(PPCRegisterList.GPR)+3*4(a3),0x0100+3*8+4(sp);  // R3
        move.l  struct(PPCRegisterList.GPR)+4*4(a3),0x0100+4*8+4(sp);  // R4
        move.l  struct(PPCRegisterList.GPR)+5*4(a3),0x0100+5*8+4(sp);  // R5
        move.l  struct(PPCRegisterList.GPR)+6*4(a3),0x0100+6*8+4(sp);  // R6
        move.l  struct(PPCRegisterList.GPR)+7*4(a3),0x0100+7*8+4(sp);  // R7
        move.l  struct(PPCRegisterList.GPR)+8*4(a3),0x0100+8*8+4(sp);  // R8
        move.l  struct(PPCRegisterList.GPR)+9*4(a3),0x0100+9*8+4(sp);  // R9
        move.l  struct(PPCRegisterList.GPR)+10*4(a3),0x0100+10*8+4(sp);// R10
        move.l  struct(PPCRegisterList.GPR)+11*4(a3),0x0100+11*8+4(sp);// R11
        move.l  struct(PPCRegisterList.GPR)+12*4(a3),0x0100+12*8+4(sp);// R12
        move.l  struct(PPCRegisterList.GPR)+13*4(a3),0x0100+13*8+4(sp);// R13
        move.l  struct(PPCRegisterList.GPR)+14*4(a3),0x0100+14*8+4(sp);// R14
        move.l  struct(PPCRegisterList.GPR)+15*4(a3),0x0100+15*8+4(sp);// R15
        move.l  struct(PPCRegisterList.GPR)+16*4(a3),0x0100+16*8+4(sp);// R16
        move.l  struct(PPCRegisterList.GPR)+17*4(a3),0x0100+17*8+4(sp);// R17
        move.l  struct(PPCRegisterList.GPR)+18*4(a3),0x0100+18*8+4(sp);// R18
        move.l  struct(PPCRegisterList.GPR)+19*4(a3),0x0100+19*8+4(sp);// R19
        move.l  struct(PPCRegisterList.GPR)+20*4(a3),0x0100+20*8+4(sp);// R20
        move.l  struct(PPCRegisterList.GPR)+21*4(a3),0x0100+21*8+4(sp);// R21
        move.l  struct(PPCRegisterList.GPR)+22*4(a3),0x0100+22*8+4(sp);// R22
        move.l  struct(PPCRegisterList.GPR)+23*4(a3),0x0100+23*8+4(sp);// R23
        move.l  struct(PPCRegisterList.GPR)+24*4(a3),0x0100+24*8+4(sp);// R24
        move.l  struct(PPCRegisterList.GPR)+25*4(a3),0x0100+25*8+4(sp);// R25
        move.l  struct(PPCRegisterList.GPR)+26*4(a3),0x0100+26*8+4(sp);// R26
        move.l  struct(PPCRegisterList.GPR)+27*4(a3),0x0100+27*8+4(sp);// R27
        move.l  struct(PPCRegisterList.GPR)+28*4(a3),0x0100+28*8+4(sp);// R28
        move.l  struct(PPCRegisterList.GPR)+29*4(a3),0x0100+29*8+4(sp);// R29
        move.l  struct(PPCRegisterList.GPR)+30*4(a3),0x0100+30*8+4(sp);// R30
        move.l  struct(PPCRegisterList.GPR)+31*4(a3),0x0100+31*8+4(sp);// R31
       
        // Set up the floating-point registers
        move.l  struct(PPCRegisterList.FPR)+0*8+0(a3),0x0200+0*8+0(sp);// FPR0;
        move.l  struct(PPCRegisterList.FPR)+0*8+4(a3),0x0200+0*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+1*8+0(a3),0x0200+1*8+0(sp);// FPR1;
        move.l  struct(PPCRegisterList.FPR)+1*8+4(a3),0x0200+1*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+2*8+0(a3),0x0200+2*8+0(sp);// FPR2;
        move.l  struct(PPCRegisterList.FPR)+2*8+4(a3),0x0200+2*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+3*8+0(a3),0x0200+3*8+0(sp);// FPR3;
        move.l  struct(PPCRegisterList.FPR)+3*8+4(a3),0x0200+3*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+4*8+0(a3),0x0200+4*8+0(sp);// FPR4;
        move.l  struct(PPCRegisterList.FPR)+4*8+4(a3),0x0200+4*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+5*8+0(a3),0x0200+5*8+0(sp);// FPR5;
        move.l  struct(PPCRegisterList.FPR)+5*8+4(a3),0x0200+5*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+6*8+0(a3),0x0200+6*8+0(sp);// FPR6;
        move.l  struct(PPCRegisterList.FPR)+6*8+4(a3),0x0200+6*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+7*8+0(a3),0x0200+7*8+0(sp);// FPR7;
        move.l  struct(PPCRegisterList.FPR)+7*8+4(a3),0x0200+7*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+8*8+0(a3),0x0200+8*8+0(sp);// FPR8;
        move.l  struct(PPCRegisterList.FPR)+8*8+4(a3),0x0200+8*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+9*8+0(a3),0x0200+9*8+0(sp);// FPR9;
        move.l  struct(PPCRegisterList.FPR)+9*8+4(a3),0x0200+9*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+10*8+0(a3),0x0200+10*8+0(sp);       // FPR10;
        move.l  struct(PPCRegisterList.FPR)+10*8+4(a3),0x0200+10*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+11*8+0(a3),0x0200+11*8+0(sp);       // FPR11;
        move.l  struct(PPCRegisterList.FPR)+11*8+4(a3),0x0200+11*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+12*8+0(a3),0x0200+12*8+0(sp);       // FPR12;
        move.l  struct(PPCRegisterList.FPR)+12*8+4(a3),0x0200+12*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+13*8+0(a3),0x0200+13*8+0(sp);       // FPR13;
        move.l  struct(PPCRegisterList.FPR)+13*8+4(a3),0x0200+13*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+14*8+0(a3),0x0200+14*8+0(sp);       // FPR14;
        move.l  struct(PPCRegisterList.FPR)+14*8+4(a3),0x0200+14*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+15*8+0(a3),0x0200+15*8+0(sp);       // FPR15;
        move.l  struct(PPCRegisterList.FPR)+15*8+4(a3),0x0200+15*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+16*8+0(a3),0x0200+16*8+0(sp);       // FPR16;
        move.l  struct(PPCRegisterList.FPR)+16*8+4(a3),0x0200+16*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+17*8+0(a3),0x0200+17*8+0(sp);       // FPR17;
        move.l  struct(PPCRegisterList.FPR)+17*8+4(a3),0x0200+17*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+18*8+0(a3),0x0200+18*8+0(sp);       // FPR18;
        move.l  struct(PPCRegisterList.FPR)+18*8+4(a3),0x0200+18*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+19*8+0(a3),0x0200+19*8+0(sp);       // FPR19;
        move.l  struct(PPCRegisterList.FPR)+19*8+4(a3),0x0200+19*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+20*8+0(a3),0x0200+20*8+0(sp);       // FPR20;
        move.l  struct(PPCRegisterList.FPR)+20*8+4(a3),0x0200+20*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+21*8+0(a3),0x0200+21*8+0(sp);       // FPR21;
        move.l  struct(PPCRegisterList.FPR)+21*8+4(a3),0x0200+21*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+22*8+0(a3),0x0200+22*8+0(sp);       // FPR22;
        move.l  struct(PPCRegisterList.FPR)+22*8+4(a3),0x0200+22*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+23*8+0(a3),0x0200+23*8+0(sp);       // FPR23;
        move.l  struct(PPCRegisterList.FPR)+23*8+4(a3),0x0200+23*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+24*8+0(a3),0x0200+24*8+0(sp);       // FPR24;
        move.l  struct(PPCRegisterList.FPR)+24*8+4(a3),0x0200+24*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+25*8+0(a3),0x0200+25*8+0(sp);       // FPR25;
        move.l  struct(PPCRegisterList.FPR)+25*8+4(a3),0x0200+25*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+26*8+0(a3),0x0200+26*8+0(sp);       // FPR26;
        move.l  struct(PPCRegisterList.FPR)+26*8+4(a3),0x0200+26*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+27*8+0(a3),0x0200+27*8+0(sp);       // FPR27;
        move.l  struct(PPCRegisterList.FPR)+27*8+4(a3),0x0200+27*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+28*8+0(a3),0x0200+28*8+0(sp);       // FPR28;
        move.l  struct(PPCRegisterList.FPR)+28*8+4(a3),0x0200+28*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+29*8+0(a3),0x0200+29*8+0(sp);       // FPR29;
        move.l  struct(PPCRegisterList.FPR)+29*8+4(a3),0x0200+29*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+30*8+0(a3),0x0200+30*8+0(sp);       // FPR30;
        move.l  struct(PPCRegisterList.FPR)+30*8+4(a3),0x0200+30*8+4(sp);
        move.l  struct(PPCRegisterList.FPR)+31*8+0(a3),0x0200+31*8+0(sp);       // FPR31;
        move.l  struct(PPCRegisterList.FPR)+31*8+4(a3),0x0200+31*8+4(sp);
       
        // This causes the 68K emulator to put us in priviledged mode and execute our routine.
        move.l  #'Gary',a0;
        move.l  #0x05051956,a1;
        move.l  #0x0000C000,d0;
        moveq   #0,d2;
        reset;
       
        move.l  sp,-(sp);
wait:
        dc.w            0xFE03;
        beq             wait;
       
        rts;
}

/************************** Code for PPC shared lib ***********************/
unsigned long _getMSR(void);

void    main(unsigned long* screenBaseAddr,unsigned long screenRowBytes,unsigned long screenHeight)
{
        // This is a priviledged instruction.  In other words, if we aren't in priviledged mode, an exception
        // will happen and are screen will never flash colors.  This is to prove that the code below is
        // actually executing in priviledged mode.
        unsigned long msr = _getMSR();
       
        // Calculate the size in longs of the screen
        long screenSize4 = screenRowBytes*screenHeight/4;
        unsigned long* ptr;
        unsigned long color = 0;
       
        // Redraw the screen over and over using a different color.  This is to show
        // that something is really happenning!
        for(;;)
        {
                ptr = screenBaseAddr;
                for(long i=0;i<screenSize4;i++)
                        *ptr++ = color;
                color += 0x01010101;
        }
       
        // Never return from here.
}

asm unsigned long _getMSR(void)
{
        // Return the contents of the machine state register.  The machine state
        // register controls everything that the processor can do.  This instruction
        // is illegal in user mode, but is allowed in priviledged mode.
        mfmsr r3;
        blr;
}
