#
#	File		NanoKernel.Make
#
#	Contains:	Makefile for the builtin/replacement multitasking kernel
#
#	Change History (most recent first):
#
#				 3/12/17	HQX		Wrote it

NanoKernelFiles	=	"{Sources}Internal:Asm:NKInfoRecordsPriv.s"		�
					"{Sources}Internal:Asm:PPCInfoRecordsPriv.s"	�
					"{Sources}Internal:Asm:MPInfoRecordsPriv.s"		�
					"{NKDir}NanoKernelEquates.s"					�
					"{NKDir}NanoKernelMacros.s"						�
					"{NKDir}NanoKernelInit.s"						�
					"{NKDir}NanoKernelReplacementInit.s"			�
					"{NKDir}NanoKernelBuiltinInit.s"				�
					"{NKDir}NanoKernelProcFlagsTbl.s"				�
					"{NKDir}NanoKernelProcInfoTbl.s"				�
					"{NKDir}NanoKernelInterrupts.s"					�
					"{NKDir}NanoKernelPaging.s"						�
					"{NKDir}NanoKernelTranslation.s"				�
					"{NKDir}NanoKernelVMCalls.s"					�
					"{NKDir}NanoKernelPowerCalls.s"					�
					"{NKDir}NanoKernelRTASCalls.s"					�
					"{NKDir}NanoKernelCacheCalls.s"					�
					"{NKDir}NanoKernelMPCalls.s"					�
					"{NKDir}NanoKernelQueues.s"						�
					"{NKDir}NanoKernelTasks.s"						�
					"{NKDir}NanoKernelAddressSpaceMPCalls.s"		�
					"{NKDir}NanoKernelPoolAllocator.s"				�
					"{NKDir}NanoKernelTimers.s"						�
					"{NKDir}NanoKernelScheduler.s"					�
					"{NKDir}NanoKernelIndex.s"						�
					"{NKDir}NanoKernelPrimaryIntHandlers.s"			�
					"{NKDir}NanoKernelConsoleLog.s"					�
					"{NKDir}NanoKernelSleep.s"						�
					"{NKDir}NanoKernelThud.s"						�
					"{NKDir}NanoKernelScreenConsole.s"				�
					"{NKDir}NanoKernelAdditions.s"					�
					


"{ObjDir}NanoKernel.s.x"		�	"{NKDir}NanoKernel.s" {NanoKernelFiles}

"{ImageDir}NanoKernel"			�	"{ObjDir}NanoKernel.s.x" "{ToolDir}XCOFF2File"
	XCOFF2File "{ObjDir}NanoKernel.s.x" {Targ}

NanoKernel						�	"{RsrcDir}NanoKernel.rsrc"
"{RsrcDir}NanoKernel.rsrc"		�	"{ImageDir}NanoKernel"
	Echo "read 'krnl' (0, locked) �"{ImageDir}NanoKernel�";" | Rez -c RSED -t rsrc -o {Targ}

CleanNanoKernel					�
	Delete -i "{ObjDir}NanoKernel.s.x" "{ImageDir}NanoKernel" "{RsrcDir}NanoKernel.rsrc" � Dev:Null

CleanEmulator					�
	Delete -i "{ObjDir}Emulator.s.x" � Dev:Null
