	include		'MacErrors.a'

	include		'PPCInfoRecordsPriv.s'
	include		'EmuInfoRecordsPriv.s'
	include		'NKInfoRecordsPriv.s'
	include		'MPInfoRecordsPriv.s'

	include		'NanoKernelEquates.s'
	include		'NanoKernelMacros.s'

	CSECT		NanoKernel[PR]
	export		NKTop, NKBtm

NKTop
	include		'NanoKernelInit.s'
	align		5
	include		'NanoKernelInterrupts.s'
	align		5
	include		'NanoKernelPaging.s'
	align		5
	include		'NanoKernelTranslation.s'
	align		5
	include		'NanoKernelVMCalls.s'
	align		5
	include		'NanoKernelPowerCalls.s'
	align		5
	include		'NanoKernelRTASCalls.s'
	align		5
	include		'NanoKernelCacheCalls.s'

	;	Mostly MP calls:
	align		5
	include		'NanoKernelMPCalls.s'
	align		5
	include		'NanoKernelQueues.s'
	align		5
	include		'NanoKernelTasks.s'
	align		5
	include		'NanoKernelAddressSpaceMPCalls.s'

	align		5
	include		'NanoKernelPoolAllocator.s'
	align		5
	include		'NanoKernelTimers.s'
	align		5
	include		'NanoKernelScheduler.s'
	align		5
	include		'NanoKernelIndex.s'
	align		5
	include		'NanoKernelPrimaryIntHandlers.s'
	align		5
	include		'NanoKernelConsoleLog.s'
	align		5
	include		'NanoKernelSleep.s'
	align		5
	include		'NanoKernelThud.s'
	align		5
	include		'NanoKernelScreenConsole.s'
	align		5
	include		'NanoKernelAdditions.s'
	align		5
NKBtm
