// simplest bit of sw ever written, but im super tired, so very bad quality
// 29th jan 16

#include <stdio.h>

void *loadfile(char *fileName, long *sizep)
{
	FILE *fp;
	long lSize;
	char *buffer;
	
	fp = fopen(fileName, "rb");
	if(!fp) return NULL;
	
	fseek(fp, 0L, SEEK_END);
	lSize = ftell(fp);
	rewind(fp);
	
	buffer = (char *)malloc(lSize+1);
	if(!buffer) {
		fclose(fp);
		return NULL;
	}
	
	*sizep = lSize;
	
	fread(buffer, 1, lSize, fp);
	fclose(fp);
	return buffer;
}

int main(int argc, char *argv[])
{
	long *buf;
	long *end;
	long len;
	
	if(argc != 2) {
		fprintf(stderr, "Usage: %s FILE\n", argv[0]);
		return 1;
	}
	
	buf = (long *)loadfile(argv[1], &len);
	if(!buf) {
		fprintf(stderr, "Could not load\n");
		return 2;
	}
	
	end = (long *)((char *)buf + len); // the hell? i need to go to bed...
	
	while(buf < end)
		printf("	DC.L	0x%08x\n", *buf++);
	
	return 0;
}