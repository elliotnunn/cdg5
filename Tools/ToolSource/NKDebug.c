#include <Types.h>
#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "NanoKernelShimLib.h"

#define page 0x1000UL


void main(void)
{
	char buf[0x2000];
	char *use;
	int result;
	int i;
	
	use = (char *)(((unsigned long)buf + 0xfffUL) & 0xfffff000UL);
	
	for(;;) {
		printf("� "); gets(use);
		
		result = NKKernelDebuggerCmd(use);
		
		if(result) {
			printf("KCKernelDebuggerCommand error: %d\n", result);
		} else {
			for(i=0; use[i]; i++) if(use[i] != 10) putchar(use[i]);
		}
	}
}
