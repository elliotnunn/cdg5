/*
	File:		LZSS.c

	Contains:	Source for an MPW tool to compress files

	Change History (most recent first):

				 1/22/16	HQX		Now a library!
*/


extern long lzss(char *dest, char *src, long src_len);
