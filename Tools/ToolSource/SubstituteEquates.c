#include <stdio.h>
#include <XCOFF.h>

#define MB 0x100000

int main(int argc, char *argv[])
{
	FILE *fp;
	long lSize;
	char *buf, *buf2;
	long ic, oc;
	char s;
	
	if(argc < 2) {
		fprintf(stderr, "Usage: %s IN [ key=value ... ] > OUT\n", argv[0]);
		fprintf(stderr, "(Note: extra arguments not matching key=value are ignored.)\n");
		return 1;
	}
	
	fp = fopen(argv[1], "rb");
	if(!fp) {
		fprintf(stderr, "Could not open input\n");
		return 2;
	}
	
	fseek(fp, 0L, SEEK_END);
	lSize = ftell(fp);
	rewind(fp);
	
	buf = (char *)malloc(lSize+1);
	buf2 = (char *)malloc(lSize+1);
	if(!buf || !buf2) {
		fprintf(stderr, "Could not open allocate\n");
		return -2;
	}
	
	fread(buf, 1, lSize, fp);
	fclose(fp);
	
	ic = 0;
	oc = 0;
	while(ic < lSize) {
		s = buf[ic++];
		if(s == '{') {				// copy it into the 
			long klen = 0;			/* key len, includes equals sign */
			long vlen = 0;			/* val len, excludes null */
			long aidx;
			
			while(ic+klen < lSize && buf[ic+klen] != '}') {
				buf2[oc+klen] = buf[ic+klen]; klen ++;
			}
			buf2[oc+klen] = '='; klen++;
			
			for(aidx=2; aidx<argc; aidx++) {
				if(!strncmp(argv[aidx], buf2+oc, klen)) {
					break;
				}
			}
			
			if(aidx < argc) {
				while(argv[aidx][klen+vlen]) {
					buf2[oc++] = argv[aidx][klen+vlen];
					vlen++;
				}
			}
			
			ic += klen;				/* skip the rest of {Key}, including } */
		} else {
			buf2[oc++] = s;
		}
	}
		
	fwrite(buf2, 1, oc, stdout);
	
	return 0;
}