#include <MacTypes.h>
#include <Resources.h>
#include <stdio.h>
#include <PEFBinaryFormat.h>
#include <CodeFragments.h>


void fail(char *msg, int ret)
{
	fprintf(stderr, "%s\n", msg);
	exit(ret);
}	


int main(int argc, char *argv[])
{
	int ret;
	
	Str255 fileName;
	unsigned char *fragName;
	int fragNameLen, fragNameLenDelta;
	
	short refnum;
	long readLen;
	
	PEFContainerHeader head;
	CFragResource *cfrgp;
	Handle cfrgh;
	Handle oldResource;
	
	
	
	if(argc != 2) goto wrongUsage;
	
	
	/* slurp  */
	
	fileName[0] = strlen(argv[1]);
	memcpy(fileName + 1, argv[1], fileName[0]);
	
	ret = FSOpen(fileName, 0, &refnum);
	if(ret) fail("Could not open file", 1);
	
	readLen = sizeof head;
	ret = FSRead(refnum, &readLen, (Ptr)(&head));
	if(ret) fail("Could not read from file", 1);
	if(readLen < sizeof head) fail("Could not read enough", 1);
	
	FSClose(refnum);
	
	
	/* need to know how long frag name is to get right resource size */
	
	fragName = fileName + fileName[0];
	while(fragName > fileName + 1 && *(fragName - 1) != ':')
		fragName--;
	
	fragNameLen = 0;
	while(fragName[fragNameLen] && fragName[fragNameLen] != '.' && fragName + fragNameLen <= fileName + fileName[0])
		fragNameLen++;

	
	/* struct CFragResource hardcodes 16b for .firstMember.name */
	
	fragNameLenDelta = (fragNameLen + 1) - sizeof cfrgp->firstMember.name;
	
	
	/* blat */
	
	cfrgh = NewHandle(sizeof *cfrgp + fragNameLenDelta);
	if(!cfrgh) fail("Out of memory!", 1);
	
	HLock(cfrgh);
	cfrgp = *(CFragResource **)cfrgh;
	
	cfrgp->reservedA		=	0;
	cfrgp->reservedB		=	0;
	cfrgp->reservedC		=	0;
	cfrgp->version			=	1;
	cfrgp->reservedD		=	0;
	cfrgp->reservedE		=	0;
	cfrgp->reservedF		=	0;
	cfrgp->reservedG		=	0;
	cfrgp->reservedH		=	0;
	cfrgp->memberCount		=	1;
	
	cfrgp->firstMember.architecture			=	head.architecture;
	cfrgp->firstMember.reservedA			=	0;
	cfrgp->firstMember.reservedB			=	0;
	cfrgp->firstMember.updateLevel			=	0; 					/* kFullLib */
	cfrgp->firstMember.currentVersion		=	head.currentVersion;
	cfrgp->firstMember.oldDefVersion		=	head.oldDefVersion;
	cfrgp->firstMember.uUsage1.appStackSize	=	0; 					/* kDefaultStackSize */
	cfrgp->firstMember.uUsage2.libFlags		=	0;
	cfrgp->firstMember.usage				=	kImportLibraryCFrag;	/* 0 */
	cfrgp->firstMember.where				=	1; 					/* kOnDiskFlat */
	cfrgp->firstMember.offset				=	0; 					/* kZeroOffset */
	cfrgp->firstMember.length				=	0; 					/* kWholeFork */
	cfrgp->firstMember.uWhere1.spaceID		=	0; 					/* addrspc?! */
	cfrgp->firstMember.uWhere2.reserved		=	0;
	cfrgp->firstMember.extensionCount		=	0;
	cfrgp->firstMember.memberSize			=	sizeof (cfrgp->firstMember)
												+ fragNameLenDelta;
	
	cfrgp->firstMember.name[0]				=	fragNameLen;
	
	memcpy(cfrgp->firstMember.name + 1,
		   (char *)fragName,
		   fragNameLen);
	
	cfrgp = NULL;
	HUnlock(cfrgh);
	
	
	/* writeout */
	
	CreateResFile(fileName);						/* doesn't hurt existing fork */
	refnum = OpenResFile(fileName);
	if(refnum <= 0) fail("Could not open resource file", 1);
	
	SetResLoad(0);
	oldResource = Get1Resource(kCFragResourceType, kCFragResourceID);
	if(oldResource) RemoveResource(oldResource);
	SetResLoad(1);
		
	AddResource(cfrgh, kCFragResourceType, kCFragResourceID, 0);
		
	return 0;										/* auto-flush */
	
	
	wrongUsage:
	
	fprintf(stderr, "Usage: %s PEF_FILE\n", argv[0]);
	return 1;
}