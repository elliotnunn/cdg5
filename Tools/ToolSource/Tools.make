#
#	File:		Tools.make
#
#	Contains:	Makefile for lost&rewritten or brand new build tools.
#				PowerPC only but sources are 68k compatible.
#
#	Change History (most recent first):
#
#				 1/29/16	HQX		Finally bootstrapped this changelog


#ToolDir			=	{Sources}Tools:
#ToolSrcDir		=	{ToolDir}ToolSource:
ToolObjDir		=	{ToolDir}Obj:


LibFiles-PPC    =	�
					"{SharedLibraries}InterfaceLib"		�
					"{SharedLibraries}StdCLib"			�
					"{SharedLibraries}MathLib"			�
					"{PPCLibraries}StdCRuntime.o"		�
					"{PPCLibraries}PPCCRuntime.o"		�
					"{PPCLibraries}PPCToolLibs.o"		�

LibFiles-PPC-SIOW =	�
	"{PPCLibraries}PPCSIOW.o" �
	"{PPCLibraries}MrCPlusLib.o" �
	"{SharedLibraries}InterfaceLib" �
	"{SharedLibraries}StdCLib" �
	"{SharedLibraries}MathLib" �
	"{PPCLibraries}PPCCRuntime.o"

LibFiles-68K    =	�
					"{Libraries}Stubs.o"				�
					"{Libraries}MathLib.o"				�
					"{CLibraries}StdCLib.o"				�
					"{Libraries}MacRuntime.o"			�
					"{Libraries}IntEnv.o"				�
					"{Libraries}ToolLibs.o"				�
					"{Libraries}Interface.o"			�


LinkOpts		= -mf -d -t 'MPST' -c 'MPS '

LinkOpts-SIOW	= -mf -d -t 'APPL' -c 'siow'


# Supplements the partial rule in the main makefile

CleanTools						�
	Delete -i `Files -f -t MPST "{ToolDir}"` `Files -f "{ToolObjDir}"` � Dev:Null


# Kinda useful supplement to MPLibrary

"{ToolObjDir}NanoKernelShimLib.s.x"		�	"{ToolSrcDir}NanoKernelShimLib.s"
	PPCAsm -o {Targ} "{ToolSrcDir}NanoKernelShimLib.s"


# Make has decent default compiler rules...

"{ToolObjDir}"					�	"{ToolSrcDir}"


# "Adler-32" produces %08X Adler-32 checksums from data forks

"{ToolDir}Adler-32"				�	"{ToolObjDir}Adler-32.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}Adler-32.c.x"


# "BinToAsm" spits out a lot of "DC.L"s

"{ToolDir}BinToAsm"				�	"{ToolObjDir}BinToAsm.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}BinToAsm.c.x"


# "DM" dumps a memory range to stdout

"{ToolDir}DM"					�	"{ToolObjDir}DM.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}DM.c.x"


# "GenerateCFrag" produces better 'cfrg's (with version info!) than MergeFragment

"{ToolDir}GenerateCFrag"		�	"{ToolObjDir}GenerateCFrag.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}GenerateCFrag.c.x"


# "NKDebug" depends on some NK hackery to hijack the NK "thud" debugger from the blue task

"{ToolDir}NKDebug"				�	"{ToolObjDir}NKDebug.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"
	PPCLink -o {Targ} {LibFiles-PPC-SIOW} {LinkOpts-SIOW} �
		"{ToolObjDir}NKDebug.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"
	Rez -a "{RIncludes}SIOW.r" -d DEFAULT_SAVE_PREF=1 -o {Targ} -t APPL -c siow


# "NKLogReader" dumps the NanoKernel log (does some tricky v->p address stuff)

"{ToolDir}NKLogReader"			�	"{ToolObjDir}NKLogReader.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"
	PPCLink -o {Targ} "{ToolObjDir}NKLogReader.c.x" "{ToolObjDir}NanoKernelShimLib.s.x" {LibFiles-PPC-SIOW} {LinkOpts-SIOW}
	Rez -a "{RIncludes}SIOW.r" -d DEFAULT_SAVE_PREF=1 -o {Targ}


# "NKxprintf" wraps syscall 96, which writes strings to the Nanokernel log

"{ToolDir}NKxprintf"			�	"{ToolObjDir}NKxprintf.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}NKxprintf.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"


# "RuntimeRomDump" extrudes the 4MB "writable toolbox aperture"
# on systems with 0x4000 in AAPL,debug
#
# Update: turns out that apart from freeing 'kckc...kc' pages,
# nothing gets changed at runtime. How disappointing.

"{ToolDir}RuntimeRomDump"			�	"{ToolObjDir}RuntimeRomDump.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}RuntimeRomDump.c.x"


# "ParcelLayout" links a boot-time parcels structure

"{ToolDir}ParcelLayout"			�	"{ToolObjDir}ParcelLayout.c.x" "{ToolObjDir}LZSS.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}ParcelLayout.c.x" "{ToolObjDir}LZSS.c.x"


# "ResRenumber" renumbers a resource of a given type and ID

"{ToolDir}ResRenumber"				�	"{ToolObjDir}ResRenumber.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}ResRenumber.c.x"


# "RiscLayout" pokes XCOFF sections into a one-megabyte binary

"{ToolDir}RiscLayout"				�	"{ToolObjDir}RiscLayout.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}RiscLayout.c.x"


# "rseg0ToDataFork" liberates data from resources

"{ToolDir}rseg0ToDataFork"				�	"{ToolObjDir}rseg0ToDataFork.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}rseg0ToDataFork.c.x"


# "SubstituteEquates" liberates data from resources

"{ToolDir}SubstituteEquates"			�	"{ToolObjDir}SubstituteEquates.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}SubstituteEquates.c.x"


# "WedgeDryRun" is the MPW interface to the Trampoline-NanoKernel Wedge

"{ToolDir}WedgeDryRun"					�	"{ObjDir}Wedge.c.x" "{ToolObjDir}NanoKernelShimLib.s.x"
	PPCLink -o {Targ} {Deps} {LibFiles-PPC-SIOW} {LinkOpts-SIOW}
	Rez -a "{RIncludes}SIOW.r" -d DEFAULT_SAVE_PREF=1 -o {Targ}


# "WedgeLogReader" dumps the Wedge log (if present)

"{ToolDir}WedgeLogReader"				�	"{ToolObjDir}WedgeLogReader.c.x"
	PPCLink -o {Targ} {Deps} {LibFiles-PPC-SIOW} {LinkOpts-SIOW}
	Rez -a "{RIncludes}SIOW.r" -d DEFAULT_SAVE_PREF=1 -o {Targ}


# "XCOFF2File" dumps the first section of an XCOFF to a data fork

"{ToolDir}XCOFF2File"				�	"{ToolObjDir}XCOFF2File.c.x"
	PPCLink -o {Targ} {LibFiles-PPC} {LinkOpts} �
		"{ToolObjDir}XCOFF2File.c.x"
