#include <stdio.h>
#include "NanoKernelShimLib.h"

int main(int argc, char **argv)
{
	if(argc != 2) {
		printf("Exactly one argument please.\n");
		return 1;
	}
	
	NKxprintf(argv[1]);
	NKxprintf("\n");
	
	return 0;
}
