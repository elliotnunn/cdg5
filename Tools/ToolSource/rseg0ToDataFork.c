#include <MacTypes.h>
#include <Resources.h>
#include <stdio.h>
#include <PEFBinaryFormat.h>
#include <CodeFragments.h>


void fail(char *msg, int ret)
{
	fprintf(stderr, "%s\n", msg);
	exit(ret);
}	


int main(int argc, char *argv[])
{
	int ret;
	
	Str255 fileName;
	
	short inRefnum, outRefnum;
	Handle hdl;
	
	long len;
	
	if(argc != 2) fail("Want two args", 1);
	
	fileName[0] = strlen(argv[1]);
	memcpy(fileName + 1, argv[1], fileName[0]);
	
	inRefnum = OpenResFile(fileName);
	if(!inRefnum) fail("Could not open rsrc fork", 2);
	
	ret = FSOpen(fileName, 0, &outRefnum);
	if(ret) fail("Could not open data fork", 3);
	
	hdl = Get1Resource('rseg', 0);
	if(!hdl) fail("rseg 0 not found", 4);
	
	len = GetHandleSize(hdl);
	FSWrite(outRefnum, &len, *hdl);
	
	return 0;
}