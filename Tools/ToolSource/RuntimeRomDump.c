#include <stdio.h>

int main(int argc, char *argv[])
{
	FILE *fp;
	
	if(argc != 2) {
		fprintf(stderr, "Usage: %s OUTPUT\n", argv[0]);
		return 1;
	}
	
	fp = fopen(argv[1], "wb");
	if(!fp) {
		fprintf(stderr, "Could not open %s\n", argv[1]);
		return 2;
	}
	
	fwrite((char *)0x60c00000, 1, 0x400000, fp);
	
	fclose(fp);
	
	return 0;
}
