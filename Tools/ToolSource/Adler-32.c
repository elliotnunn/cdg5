#include <stdio.h>

int main(int argc, char *argv[])
{
	unsigned char *buf;
	unsigned long bufsize = 0x1000000;
	unsigned long bufgot, i;
	FILE *fp;
	
	unsigned short A=1, B=0;
	
	if(argc != 2) {
		fprintf(stderr, "Usage: %s FILE\n", argv[0]);
		return 1;
	}
	
	fp = fopen(argv[1], "rb");
	if(!fp) {
		fprintf(stderr, "Could not open %s\n", argv[1]);
		return 1;
	}
	
	/* Try for 16mb and halve until allocation succeeds */
	
	for(;;) {
		buf = (unsigned char *)malloc(bufsize);
		if(buf) break;
		bufsize >>= 1;
	}
	
	do {
		bufgot = fread((void *)buf, 1, bufsize, fp);
		
		for(i=0; i<bufgot; i++) {
			A = (A + buf[i]) % 65521;
			B = (B + A) % 65521;
		}
			
	} while(bufgot == bufsize);
	
	printf("%08X", ((unsigned long)B) << 16 | (unsigned long)A);
	
	return 0;
}
	