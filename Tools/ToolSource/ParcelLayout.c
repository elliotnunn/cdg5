/*
	File:		ParcelLayout.c
	
	Contains:	Source to tool that mashes together PowerPC binaries to be
				injected into the Open Firmware device tree by the MacOS
				bootloader or Classic Environment.
				
				Parcels are present in Mac OS ROM files starting with v2.x.
	
	Change History (most recent first):
	
				 1/29/17	HQX		Support loading data from resources.
				 1/11/17	HQX		Replaced CRC code with something that works.
				  1/8/17	HQX		Wrote it.
*/


#include <MacMemory.h>
#include <MacTypes.h>
#include <MacErrors.h>
#include <TextUtils.h>
#include <Files.h>
#include <Finder.h>
#include <stdio.h>
#include <string.h>
#include "LZSS.h"


#define		Pull(src, typ)		(*(typ *)(((src) += sizeof (typ)) - sizeof (typ)))
#define 	Next(dest, typ) 	(*(typ *)((dest += sizeof (typ)) - sizeof (typ)))
#define		Cmp(ua, ub)			(!memcmp(&(ua), &(ub), sizeof(ua)))
#define		MAX_SIZE			0x400000
#define		LAYOUT_RES_TYPE		'prcl'


/* At the top of each 'prcl' resource in Parcels.rsrc */

struct layoutHead {
	long type;
	char strings[64];
	long flags;
	long count;
};

struct refSrc {
	long key;			/* 'rsrc' or 'data' */
	Str255 fileName;
	long type;
	Str255 resName;
};

struct litSrc {
	long type;			/* 'cstr' or 'csta', but others are acceptable */
	long len;
	char data[512];
};

union layoutSrc {
	long key;			/* overlaps with ref.key and lit.type: 'data', 'rsrc', 'cstr', 'csta' */
	struct refSrc ref;
	struct litSrc lit;
};

struct layoutEntry {
	char string[32];
	long flags;
	union layoutSrc src;
};

struct cacheEntry {
	union layoutSrc src;									/* key */	
	
	long offset, type, finalLen, encodLen, compress;		/* values */
};


/* Useful globals */

OSErr ret;


unsigned long crc32(unsigned long crc, unsigned char *buf, long len)
{
    int k;

    crc = ~crc;
    while (len--) {
        crc ^= *buf++;
        for (k = 0; k < 8; k++)
            crc = crc & 1 ? (crc >> 1) ^ 0xedb88320 : crc >> 1;
    }
    return ~crc;
}


void fail(char *msg, int retCode)
{
	fprintf(stderr, "**** Hiram Error: %s", msg);
	if (retCode)
		fprintf(stderr, " ; Return Code = %d\n", retCode);
	else
		fprintf(stderr, "");
	exit(1);
}


int main(int argc, char *argv[])
{
	/* source */
	Str255 layoutFileName;
	short layoutRefnum;
	short parcelCount;
	
	/* intermediate */
	char *image, *head;
	long *nextOv = 0;		/* ptr to long in prev parcel with point to next parcel */
	struct cacheEntry cache[128]; int cache_n = 0;
	long i, j;
	
	/* dest */
	Str255 destFileName;
	short destRefnum;
	long imageSize;
	
	/* Deal with args. */
	
	if(argc != 3) {
		fprintf(stderr, "Usage: %s LAYOUT.RSRC OUT.IMAGE\n", argv[0]);
		return 1;
	}
	
	*layoutFileName = strlen(argv[1]);
	memcpy(layoutFileName+1, argv[1], *layoutFileName);
	*destFileName = strlen(argv[2]);
	memcpy(destFileName+1, argv[2], *destFileName);
	
	
	/* Do we have a hope of writing this out? */
	
	ret = Create(destFileName, 0, 'MPS ', 'prcl');
	if(ret && ret != dupFNErr) fail("Could not create file", ret);
	
	ret = FSOpen(destFileName, 0, &destRefnum);
	if(ret) fail("Could not open dest file", ret);
	
	
	/* 4 MB is already too much for Open Firmware to handle! */
	
	head = image = NewPtr(MAX_SIZE);
	if(!image) fail("Could not allocate image", 0);
	memset(image, 0, MAX_SIZE);
	
	
	/* Set up the basic parcel header: don't know what it means, but it never changes */
	
	Next(head, long) = LAYOUT_RES_TYPE;
	Next(head, long) = 0x01000000;
	Next(head, long) = 0x14;
	Next(head, long) = 0x14;
	Next(head, long) = 0;
	
	
	/* All our input comes from this one resource file */
	
	layoutRefnum = OpenResFile(layoutFileName);
	if(layoutRefnum <= 0) fail("Could not open layout file", layoutRefnum);
	
	parcelCount = CountResources(LAYOUT_RES_TYPE);
	
	for(i=1; i<=parcelCount; i++) {
		/* these all help access the contents of the resource */
		Handle layoutResHdl;
		struct layoutHead *layout_headp;
		struct layoutEntry *layout_entryp;
		short layoutResID;
		Str255 layoutResName;
		
		/* a temporary ptr to a destination for binaries after the parcel header and entries */
		char *datahead;
		
		layoutResHdl = (Handle)GetIndResource(LAYOUT_RES_TYPE, i);
		if(!layoutResHdl) fail("Could not get resource", 0);
		HLock(layoutResHdl);
		layout_headp = (struct layoutHead *)(*layoutResHdl);
		
		GetResInfo(layoutResHdl, &layoutResID, 0, layoutResName);
		printf("resource 'prcl' (%d, \"%.*s\")\n", layoutResID, *layoutResName, layoutResName+1);
		
		if(nextOv) *nextOv = (long)(head - image);
		nextOv = (long *)head;
		
		Next(head, long) = 0; // overwrite when another prcl added
		Next(head, long) = layout_headp->type;
		Next(head, long) = 88 + 60*layout_headp->count;
		Next(head, long) = layout_headp->flags;
		Next(head, long) = layout_headp->count;
		Next(head, long) = 60;
		memcpy(head, layout_headp->strings, sizeof(layout_headp->strings));
		head += sizeof(layout_headp->strings);
		
		/* find somewhere to dump data, well clear of the structures */
		/* if you blat something @ datahead, you must increment it! */
		datahead = head + 60*layout_headp->count;
		
		/* and start reading fixed-size entries... */
		layout_entryp = (struct layoutEntry *)(*layoutResHdl + sizeof *layout_headp);
		
		/* now we fill in some entries */
		
		for(j=0; j<layout_headp->count; j++, layout_entryp++) {
			int cache_i;
			
			/* these vars need to be retrieved from the cache or otherwise calculated */
			long offset = 0, type, finalLen, encodLen, compress;
			
			/* do a cache search straight away! */
			for(cache_i=cache_n-1; cache_i>=0; cache_i--)
			{
				if(Cmp(cache[cache_i].src, layout_entryp->src)) {
					offset		=	cache[cache_i].offset;
					type		=	cache[cache_i].type;
					finalLen	=	cache[cache_i].finalLen;
					encodLen	=	cache[cache_i].encodLen;
					compress	=	cache[cache_i].compress;
					break;
				}
			}
			
			if(!offset) {		/* in this block, must set: offset, compress, finalLen, encodLen, type */
				offset = (long)(datahead - image);									/* set offset for all */
				
				if(layout_entryp->src.key == 'data') {	
					Ptr temp;
					short dataRefnum;
					FInfo finf;
					
					printf("	DataFork: \"%.*s\"\n", *layout_entryp->src.ref.fileName, layout_entryp->src.ref.fileName+1);
					
					compress = 'lzss';												/* set compress */
					
					ret = GetFInfo(layout_entryp->src.ref.fileName, 0, &finf);
					if(ret) fail("Could not get file type", ret);
					type = finf.fdType;												/* set type */
					
					ret = FSOpen(layout_entryp->src.ref.fileName, 0, &dataRefnum);
					if(ret) fail("Could not open big file", ret);
					
					ret = GetEOF(dataRefnum, &finalLen);							/* set finalLen */
					if(ret) fail("Could not get size of big file", ret);
					
					temp = NewPtr(finalLen);
					if(!temp) fail("Could not allocate temp buffer", 1);
					
					ret = FSRead(dataRefnum, &finalLen, temp);
					if(ret) fail("Could not read big file", ret);
					
					encodLen = lzss(datahead, temp, finalLen);						/* set encodLen */
					datahead += encodLen;
										
					ret = FSClose(dataRefnum);
					if(ret) fail("Could not close file", ret);
					
				} else if (layout_entryp->src.key == 'rsrc') {
					Handle temp;
					short resRefnum;
					
					printf("	Resource: ");
					printf("\"%.*s\" '%.4s' \"%.*s\"\n",			*layout_entryp->src.ref.fileName, layout_entryp->src.ref.fileName+1,
															  		&layout_entryp->src.ref.type,
															  		*layout_entryp->src.ref.resName, layout_entryp->src.ref.resName+1);
					
					compress = 'lzss';												/* set compress */
					type = layout_entryp->src.ref.type;								/* set type */
					
					resRefnum = OpenResFile(layout_entryp->src.ref.fileName);
					if(resRefnum <= 0) fail("Could not open resource file", 1);
					
					temp = (Handle)Get1NamedResource(layout_entryp->src.ref.type, layout_entryp->src.ref.resName);
					if(!temp) fail("Could not get resource from file", 1);
					
					finalLen = GetHandleSize(temp);									/* set finalLen */		
					
					encodLen = lzss(datahead, *temp, finalLen);						/* set encodLen */
					datahead += encodLen;
					
					CloseResFile(resRefnum);
					
				} else {
					printf("	Raw data: '%.4s'\n", &(layout_entryp->src.lit.type));
					
					compress = 0;													/* set compress */
					type = layout_entryp->src.lit.type;								/* set type */
					
					finalLen = layout_entryp->src.lit.len;							/* set finalLen */
					
					memcpy(datahead, layout_entryp->src.lit.data, finalLen);
					datahead += finalLen;
					
					encodLen = finalLen;											/* set encodLen */
				}
				
				/* and pop what we've found in the cache! */
				if(cache_n < sizeof cache / sizeof *cache) {	/* not a great time to start bounds-checking */
					cache[cache_n].src				=	layout_entryp->src;
					cache[cache_n].offset			=	offset;
					cache[cache_n].type				=	type;
					cache[cache_n].finalLen			=	finalLen;
					cache[cache_n].encodLen			=	encodLen;
					cache[cache_n].compress			=	compress;
					cache_n++;
				}
			}
			
			
			Next(head, long) = type;
			Next(head, long) = layout_entryp->flags;
			Next(head, long) = compress;
			Next(head, long) = finalLen;
			Next(head, long) = crc32(0, (unsigned char *)(image+offset), encodLen);
			Next(head, long) = encodLen;
			Next(head, long) = offset;
			
			if(layout_entryp->src.key == 'rsrc' && layout_entryp->string[0] == 0 && layout_entryp->string[1] == '~') {
				/* use the name of the resource as the name string */
				int strlen = *layout_entryp->src.ref.resName;
				if(strlen > sizeof layout_entryp->string) strlen = sizeof layout_entryp->string;
				memcpy(head, layout_entryp->src.ref.resName + 1, strlen);
			} else {
				memcpy(head, layout_entryp->string, sizeof layout_entryp->string);
			}
			head += sizeof layout_entryp->string;
			
			datahead = (char *)(((long)datahead + 3) & -4);
		}
		
		head = datahead; /* get clear of the data we blatted */
		
		DisposeHandle(layoutResHdl);
	} /* now on to the next parcel */
	
	/* write out */
	
	ret = SetEOF(destRefnum, 0);
	if(ret) fail("Could not trunc dest file", ret);
	
	imageSize = (long)(head - image);
	ret = FSWrite(destRefnum, &imageSize, image);
	if(ret) fail("Could not write to dest file", ret);
	
	ret = FSClose(destRefnum);
	if(ret) fail("Could not close dest file", ret);
	
	free(image);
	
	return 0;
}



