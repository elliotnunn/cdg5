#include <MacTypes.h>
#include <Resources.h>
#include <stdio.h>


void fail(char *msg, int ret)
{
	fprintf(stderr, "%s\n", msg);
	exit(ret);
}	


int main(int argc, char *argv[])
{
	Str255 fileName;
	short refnum;
	short oldID, newID;
	long typ;
	int ret;
	Handle res;
	
	if(argc != 5) goto wrongUsage;
	
	ret = sscanf(argv[1], "%hd", &oldID);
	if(ret != 1) goto wrongUsage;
	
	if(strlen(argv[2]) != 4) goto wrongUsage;
	typ = *(long *)argv[2];
	
	ret = sscanf(argv[3], "%hd", &newID);
	if(ret != 1) goto wrongUsage;
	
	memcpy(fileName+1, argv[4], fileName[0] = strlen(argv[4]));
	
	refnum = OpenResFile(fileName);
	if(refnum <= 0) fail("Could not open file", 1);
	
	SetResLoad(0);
	
	res = Get1Resource(typ, oldID);
	if(!res) fail("Could not open resource", 1);
	
	SetResInfo(res, newID, 0);
	
	ChangedResource(res);
	
	CloseResFile(refnum);
	
	return 0;
	
	
	wrongUsage:
	
	fprintf(stderr, "Usage: %s OLD_ID TYPE NEW_ID FILE\n", argv[0]);
	return 1;
}