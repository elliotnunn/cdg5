#include <stdio.h>
#include <XCOFF.h>
#include <MacTypes.h>

#define MB 0x400000

struct sums {
	UInt32 bytesums[8];
	UInt64 widesum;
};

long getblob(char *fileName, char **bufpp)
{
	FILE *fp;
	FileHdr xc_head;
	
	fp = fopen(fileName, "rb");
	if(!fp) return -1;
	
	fread(&xc_head, 1, sizeof xc_head, fp);			/* first, is this open file an XCOFF? */
	
	if(!feof(fp) && xc_head.f_magic == U802TOCMAGIC) {	/* YES, XCOFF */
		SectionHdrEntry xc_sec;
		
		/* we have an xcoff! */
		//if(xc_head.f_nscns != 0) fprintf(stderr, "(XCOFF) %s has %d (!= 1) sections.\n", fileName, xc_head.f_nscns+1);
		
		fseek(fp, xc_head.f_opthdr, SEEK_CUR);			/* skip the aux header */
		
		fread(&xc_sec, 1, sizeof xc_sec, fp);
		if(feof(fp)) return -2;							/* couldnt get a section header entry */
		
		*bufpp = (char *)malloc(xc_sec.s_size);
		if(!*bufpp) return -3;							/* oom */
		
		fseek(fp, xc_sec.s_scnptr, SEEK_SET);
		fread(*bufpp, 1, xc_sec.s_size, fp);
		if(feof(fp)) return -4;							/* could not read section */
		
		return xc_sec.s_size;
		
	} else {											/* NO, RAW FILE */
		long size;
		
		fseek(fp, 0, SEEK_END);							/* apparently this passes for an idiom in C */
		size = ftell(fp);
		
		*bufpp = (char *)malloc(size);
		if(!*bufpp) return -3;							/* oom */
		
		fseek(fp, 0, SEEK_SET);
		fread(*bufpp, 1, size, fp);
		if(feof(fp)) return -4;							/* could not read section */
		
		return size;
	}
}

int main(int argc, char *argv[])
{
	char *megabyte;
	UInt8 *bytearray;
	UInt64 *widearray;
	
	int first_offset_arg, first_path_arg, filec;
	long ret;
	
	FILE *fp;
	
	struct sums sums, *sumdest;
	long i, j;
	
	
	/* special linker stuff */
	//long loMemInitOffset = -1;
	
	
	if(argc < 2 || argc & 1) {
		fprintf(stderr, "Usage: %s OUT [N x OFFSET ...] [N x FILE ...]\n", argv[0]);
		return 1;
	}
	
	
	/* Offset args are all together, and before the filename args */
	/* Files can be XCOFF (magic number checked) or raw */
	
	filec = (argc - 2) >> 1;
	first_offset_arg = 2;
	first_path_arg = 2 + filec;
	
	megabyte = (char *)malloc(MB);
	if(!megabyte) {
		fprintf(stderr, "OOM\n");
		return 2;
	}
	memset(megabyte, 0, MB);			/* calloc is dodgy? */
	
	
	/* blat sections into place */
	
	for(i=0; i<filec; i++) {
		char *filename = argv[first_path_arg+i];
		char *offsetarg = argv[first_offset_arg+i];
		
		long offset, realoffset;
		long size;
		char *bufp;
		int underneath;
		
		offset = strtol(offsetarg, /*endptr*/ NULL, /*base*/ 0);
		if(offset == -1L) continue;
		
		underneath = (offset < 0);
		if(underneath) offset = -offset;
		
		size = getblob(filename, &bufp);
		if(size <= 0) {
			fprintf(stderr, "No can load\n");
			return 3;
		}
		
		realoffset = offset;
		if(underneath) realoffset -= size;
		
		if(realoffset + size > MB) {
			fprintf(stderr, "Binary too big\n");
			return 4;
		}
		
		//if(underneath && offset == 0x30e000) loMemInitOffset = realoffset - 0x30d000;
		
		memcpy(megabyte + realoffset, bufp, size);
		
		free(bufp);
	}
	
	
	/* special linker stuff! */
	
	//*(long *)(megabyte + 0x30d0b0) = loMemInitOffset;
	
	sumdest = (struct sums *)(megabyte + 0x30d000);
	memset(sumdest, 0, sizeof *sumdest);
	memset(&sums, 0, sizeof *sumdest);
	
	bytearray = (UInt8 *)megabyte;
	widearray = (UInt64 *)megabyte;
	
	for(i=0; i<MB/sizeof(*bytearray); i+=8) {
		for(j=0; j<8; j++) {
			sums.bytesums[j] += bytearray[i+j];
		}
	}
	
	for(i=0; i<MB/sizeof(*widearray); i++) {
		sums.widesum += widearray[i];
	}
	
	memcpy(sumdest, &sums, sizeof sums);
	
	fp = fopen(argv[1], "wb");
	if(!fp) {
		fprintf(stderr, "Could not open dest\n");
		return 4;
	}
	
	ret = fwrite(megabyte, 1, MB, fp);
	if(ret != MB) {
		fprintf(stderr, "Only wrote %l\n", ret);
		return 5;
	}
	
	fclose(fp);
	
	return 0;
}