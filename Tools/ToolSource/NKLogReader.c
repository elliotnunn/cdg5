#include <MacTypes.h>
#include <MacErrors.h>
#include <Events.h>
#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "NanoKernelShimLib.h"




char *LA_KernelData, *PA_KernelData, *LA_Ctr;
unsigned long toP, toL;
int ready_for_update = 0;




char *Get_LA_Head()
{
	char *p;
	p = (char *)*(unsigned long *)(LA_KernelData - 0x404);
	if(p) p += toL;
	return p;
}




char Get_NK_Char()
{
	char c;

	c = *LA_Ctr++;

	if (((unsigned long)LA_Ctr & 0xfff) == 0) {
		LA_Ctr = (char *)*(unsigned long *)(LA_Ctr - 0x1000) + toL;
	}

	return c;
}




extern Boolean my_callback(EventRecord *er)
{
	if(!ready_for_update || er->what != 0) return 0;

	while(LA_Ctr != Get_LA_Head()) {
		char c = Get_NK_Char();
		if(c && c != 10) putchar(c);
	}

	return 0;
}
typedef Boolean (*my_callback_t)(EventRecord *er);
extern my_callback_t __siowEventHook = &my_callback;




void main(void)
{
	OSErr err;
	long nk_struct_ver, nk_struct_len;

	printf("NKLogReader by Elliot Nunn, 2017 <elliotnunn@fastmail.com>.\n");

	err = NKLocateInfoRecord(kNKNanoKernelInfo, &LA_KernelData, &nk_struct_ver, &nk_struct_len);

	if(err == unimpErr) {
		printf("MP call NKLocateInfoRecord returned unimpErr.\nThe multitasking NanoKernel is probably not installed. Stopping.\n");
		return;
	} else if(err == paramErr) {
		printf("MP call NKLocateInfoRecord returned paramErr.\nStopping.\n");
		return;
	} else if(err) {
		printf("MP call NKLocateInfoRecord returned unexpected error %d.\nStopping.\n", err);
		return;
	}
	
	printf("NKNanoKernelInfo: logi addr %08x, struct ver %04x, %d bytes.\n", LA_KernelData, nk_struct_ver, nk_struct_len);
	LA_KernelData = (char *)((unsigned long)LA_KernelData & 0xfffff000UL);

	PA_KernelData = (char *)*(unsigned long *)(LA_KernelData - 4);
	if(!PA_KernelData || (unsigned long)PA_KernelData == 0x68f168f1UL) {
		printf("Did NOT find KernelData ptr at (KernelData - 4) [i.e. Exception Work Area].\nStopping.\n");
		return;
	}
	printf("NKNanoKernelInfo is within the Kernel Data Page. KDP phys addr %08x.\n", PA_KernelData);

	toP = (unsigned long)PA_KernelData - (unsigned long)LA_KernelData;
	toL = (unsigned long)LA_KernelData - (unsigned long)PA_KernelData;

	LA_Ctr = Get_LA_Head();
	if(!LA_Ctr) {
		printf("Primary System Area log pointer is 0.\nThis NanoKernel is logless. Stopping.\n");
		return;
	}

	printf("NanoKernel log starts after one blank line.\n\n");

	do { // LA_Ctr equals a value that we haven't read yet
		char c = Get_NK_Char();
		if(c && c != 10) putchar(c);
	} while(LA_Ctr != Get_LA_Head());

	ready_for_update = 1;

	for (;;) getchar(); // wish this didn't have to write...
}
