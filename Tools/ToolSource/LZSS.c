/*
	File:		LZSS.c

	Contains:	Source for an MPW tool to compress files

	Change History (most recent first):

				 1/22/16	HQX		Now an include file instead of a Tool
				 1/10/16	HQX		Fixed horrible bug
				 12/?/16	HQX		Last LZSS compressor I *ever* write
*/


#include "LZSS.h"


/* I want the largest buffer possible, with dest at bottom and src at top */
long lzss(char *dest, char *src, long src_len)
{
	char *dest_btm = dest;
	char *src_btm = src;
	char *src_top = src + src_len;
	char *head;
	
	unsigned int units = 0;
	
	while(src_top > src) {
		int push_n = 1; // this is zero for ones?
		int push_d = 1;
		
		int best_push_n = 0;
		int best_push_d;
		
		int test_n;
		int test_d;
		
		if((unsigned long)src & 3) goto skip;
		
		for(test_d=4; src-test_d>=src_btm && test_d <= 350; test_d+=4) {
			for(test_n=0;
				test_n<16 && test_n<test_d && *(long *)(src+test_n) == *(long *)(src-test_d+test_n);
				test_n+=4) {}
/*			if(test_n == 16 && test_d>=18 && *(short *)(src+test_n) == *(short *)(src-test_d+test_n))*/
/*				test_n += 2;*/
			if(test_n > best_push_n) {best_push_n = test_n; best_push_d = test_d;}
			if(test_n == 16) break;
		}
		
		if(best_push_n) {push_n = best_push_n; push_d = best_push_d;}
		
		skip:
		
		if(!(units & 7)) {
			*(head = dest++) = 0;
			//fprintf(stderr, "- NEW HEAD - 0x%06x\n", head-dest_btm);
		}
		*head |= (push_n == 1) << (units++ & 7);
		
		if(push_n == 1) {
			//fprintf(stderr, "0x%06x - > 0x%06x : wrote 0x%02x\n", src-src_btm, dest-dest_btm, (unsigned char)*src);
			*dest++ = *src++;
		} else {
			unsigned int ptr = src - src_btm + 0xfee - push_d;
			*dest++ = ptr & 0xff;
			*dest++ = (push_n - 3) | ((ptr & 0xf00) >> 4);
			src += push_n;
			//fprintf(stderr, "0x%06x + > 0x%06x : wrote 0x%02x%02x : means %d b from minus %d\n", (src-push_n-src_btm), (dest-dest_btm-2), (unsigned char)*(dest-2), (unsigned char)*(dest-1), push_n, push_d);
		}
		
		//return dest - dest_btm;
	}
	
	return dest - dest_btm;
}

/*
int main(int argc, char **argv)
{
	short src_refnum;
	char *buf;
	long src_len, dest_len, buf_len;
	long ret;
	char pn[512];
	
	FILE *outf;
	
	if(argc != 3) fail("Wrong arg count", argc);
	
	strcpy(pn, argv[1]);
	ret = FSOpen((unsigned char *)c2pstr(pn), 0, &src_refnum);
	if(ret) fail("Could not open src", ret);
	
	ret = GetEOF(src_refnum, &src_len);
	if (ret) fail("Failed GetEOF on src", ret);
	
	buf_len = MaxBlock();
	//printf("biggest block = 0x%d\n", buf_len);
	buf = NewPtr(buf_len);
	if(!buf) fail("Could not allocate biggest block", 1);
	
	ret = FSRead(src_refnum, &src_len, buf + buf_len - src_len);
	if(ret) fail("Could not read", ret);
	
	ret = FSClose(src_refnum);
	if(ret) fail("Could not close src", ret);
	
	dest_len = compress(buf, buf + buf_len - src_len, src_len);
	
	SetPtrSize(buf, dest_len);
	
	outf = fopen(argv[2], "wb");
	if(!outf) fail("Could not open dest", 1);
	
	fwrite(buf, dest_len, 1, outf);
	fclose(outf);
	
	return 0;
}
*/