#include <stdio.h>
#include <XCOFF.h>

#define MB 0x100000

int main(int argc, char *argv[])
{
	FILE *fp;
	long lSize;
	char *buf;
	FileHdrPtr fhp;
	SectionHdrEntryPtr shp;
	
	if(argc != 3) {
		fprintf(stderr, "Usage: %s IN OUT\n", argv[0]);
		return 1;
	}
	
	fp = fopen(argv[1], "rb");
	if(!fp) {
		fprintf(stderr, "Could not open input\n");
		return 2;
	}
	
	fseek(fp, 0L, SEEK_END);
	lSize = ftell(fp);
	rewind(fp);
	
	buf = (char *)malloc(lSize+1);
	if(!buf) {
		fprintf(stderr, "Could not open allocate\n");
		return -2;
	}
	
	fread(buf, 1, lSize, fp);
	fclose(fp);
	
	fhp = (FileHdrPtr)buf;
	
	shp = (SectionHdrEntryPtr)(buf + sizeof *fhp + fhp->f_opthdr);
	
	fp = fopen(argv[2], "wb");
	if(!fp) {
		fprintf(stderr, "Could not open output\n");
		return 1;
	}
	
	fwrite(buf + shp->s_scnptr, 1, shp->s_size, fp);
	
	fclose(fp);
	
	return 0;
}