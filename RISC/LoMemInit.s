;
;	File:		LoMemInit.s
;
;	Contains:	Key/value pairs for initializing Low Memory Globals.
;
;	Change History (most recent first):
;
;	 			 2/10/17	HQX		Separate out from ConfigInfo



;			LoMem addr		Value	

	DC.L	0x00000004,		0xffc0002a



; sentinel
	DC.L	0