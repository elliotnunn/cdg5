#
#	File		RISC.Make
#
#	Contains:	Makefile for 4 MB RISC ROM image (wraps 3 MB 68k ROM image)
#
#	Change History (most recent first):
#
#				  2/9/17	HQX		Improved ConfigInfo reversal
#				 1/29/17	HQX		Wrote it.

RISCObjects		=	"{ImageDir}RomMondo"					�
					"{ObjDir}ExceptionTable.s.x"			�
					"{ObjDir}ConfigInfo.s.x"				�
					"{ObjDir}LinkedWedge.x"					�
					"{ObjDir}NanoKernel.s.x"				�
					"{ObjDir}Emulator.s.x"					�
					"{ObjDir}DispatchTable.s.x"				�



RISCOffsets		=	 0 																�
					 0x300000 														�
					 0x30d000														�
					 `If {WedgeBool}; Echo -n 0x310000; Else; Echo -n -1; End`		�
					 `If {WedgeBool}; Echo -n 0x340000; Else; Echo -n 0x310000;End`	�
					 0x360000														�
					 0x380000														�



# Assemble the objects!

"{ObjDir}"						�	"{RiscDir}"


# Wedgie!

"{ObjDir}LinkedWedge.x"			�	"{ObjDir}WedgeStub.s.x" "{ObjDir}printf.c.x" "{ObjDir}Wedge.c.x"
	PPCLink -outputformat xcoff -codeorder source -roistext on {Deps} -o {Targ}


# And link them!

RiscMondo						�	"{ImageDir}RiscMondo"

"{ImageDir}RiscMondo"			�	"{ToolDir}RiscLayout" {RISCObjects}
	Set WedgeBool `Echo {FeatureSet} | StreamEdit -e "1 Replace /�� Wedge=/ ''" -e "1 Replace / Ű/ ''"`
	RiscLayout {Targ} {RISCOffsets} {RISCObjects}
	SetFile -t 'rom ' {Targ}


#	Do not delete RomMondo! And should I really be deleting the NanoKernel? Yes, probably.
CleanRisc						�
	Delete -i "{ImageDir}RiscMondo" {RiscObjects} � Dev:Null
