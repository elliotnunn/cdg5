# Finally got this making (semi-)bootable images around midnight on 10/11 Jan 2017. Phew.

Main							�	MacOSROM

#_______________________________________________________________________
#	OUTPUTS
#_______________________________________________________________________

BuildDir		=	{Sources}BuildResults:NewWorld:
ImageDir		=	{BuildDir}Image:
RsrcDir			=	{BuildDir}Rsrc:
ObjDir			=	{BuildDir}Obj:
TextDir			=	{BuildDir}Text:
MiscDir			=	{BuildDir}Rsrc:


#_______________________________________________________________________
#	INPUTS
#_______________________________________________________________________

Sources			=	:
MakeDir			=	{Sources}Make:
ResourceDir		=	{Sources}Resources:
ToolDir			=	{Sources}Tools:
ToolSrcDir		=	{Sources}Tools:ToolSource:
PrebuiltDir     =   {Sources}Prebuilt:LateNewWorld:
NewWorldDir		=	{Sources}NewWorld:
TrampolineDir	=	{NewWorldDir}Trampoline:
RiscDir			=	{Sources}RISC:
NKDir			=	{Sources}NanoKernel:


#_______________________________________________________________________
#	Additions to MPW Make's default rules
#_______________________________________________________________________

# 68k
AOptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:Asm:"    {FeatureSet} -d Alignment=16 -d CPU=40 -d ROMRelease={ROMVersion} -wb -d SubRelease={SubVersion}
COptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:C:"      {FeatureSet} -d Alignment=16 -d CPU=40 -d ROMRelease={ROMVersion} -b3 -mbg full -mc68020
POptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:Pascal:"                {FeatureSet} -mbg full -mc68020
PPCAOptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:Asm:"    {FeatureSet} -w off -i "{AIncludes}" {RiscAddrSet}
PPCCOptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:C:"      {FeatureSet}
ROptions		=	-d TRUE=1 -d FALSE=0 -i "{Sources}Internal:Rez:"    {FeatureSet} -d RomBase={RomBase}

# Make is missing a default Rez build rule:
.rsrc							�	.r
	Rez {depDir}{default}.r -o {targDir}{default}.rsrc {ROptions}


#_______________________________________________________________________
#	OPTIONS for the build
#	(Features get processed at runtime into build-ready FeatureSet)
#_______________________________________________________________________

RomBase			=	$FFC00000
ROMVersion		=	$45F6
SubVersion		=	$0001

RiscAddrSet		=	-d LA_InfoRecordPage=0x5fffe000

Features		=	hasSCSIDiskModeFeature					�
					hasEDisk								�
					hasAMIC									�
					NewWorld								�
					hasPCI									�
					hasMixedMode							�
					cdg5HappyMac							�

FeatureSet						�
	Set -e FeatureSet			"`{MakeDir}FeatureList "{Features}" "{Overrides}"`"
	Set -e Commands				"{ToolDir},{Commands}"
	Set -e ROMBuildTime 		"`Date -n`"
	Set -e ImageDir				"{ImageDir}"	# vars to be exported to Rez...
	Set -e RsrcDir				"{RsrcDir}"
	Set -e ObjDir				"{ObjDir}"
	Set -e TextDir				"{TextDir}"
	Set -e MiscDir				"{MiscDir}"


#_______________________________________________________________________
#	PARCELS (Trampoline metadata for device tree)
#	(Features get processed at runtime into build-ready FeatureSet)
#_______________________________________________________________________

#
# As above, when you add a file to "ParcelFiles", you must
# likewise add 'prcl' entries to :NewWorld:Parcels.r.
#
ParcelFiles		=	�
					"{ImageDir}RiscMondo"					�
					"{RsrcDir}ATA.rsrc"						�
					"{RsrcDir}Display.rsrc"					�
					"{RsrcDir}FireWire.rsrc"				�
					"{RsrcDir}I2C.rsrc"						�
					"{RsrcDir}MiscLibs.rsrc"				�
					"{RsrcDir}Network.rsrc"					�
					"{RsrcDir}NVRAM.rsrc"					�
					"{RsrcDir}PCCard.rsrc"					�
					"{RsrcDir}PCI.rsrc"						�
					"{RsrcDir}PowerManagement.rsrc"			�
					"{RsrcDir}RTC.rsrc"						�


#_______________________________________________________________________
#	RESOURCES
#	(Mostly extracted)
#_______________________________________________________________________

# This reconstitutes most of the DeRezzed "prebuilt" resources

"{RsrcDir}"								�	"{PrebuiltDir}"


#_______________________________________________________________________
#	The "MAC OS ROM" TOOLBOX IMAGE
#	(Boots Open Firmware machines. Rules are here because it is our
#	final product.)
#_______________________________________________________________________

# Ugly ugly ugly

"{ObjDir}NQDResidentCursor"				�	"{PrebuiltDir}SystemEnabler:NQDResidentCursor.r" "{ToolDir}rseg0ToDataFork" "{ToolDir}GenerateCFrag"
	Rez "{PrebuiltDir}SystemEnabler:NQDResidentCursor.r" -o {Targ}
	rseg0ToDataFork {Targ}
	GenerateCFrag {Targ}
	
"{ObjDir}ProcessMgrLib"					�	"{PrebuiltDir}SystemEnabler:ProcessMgrLib.r" "{ToolDir}rseg0ToDataFork" "{ToolDir}GenerateCFrag"
	Rez "{PrebuiltDir}SystemEnabler:ProcessMgrLib.r" -o {Targ}
	rseg0ToDataFork {Targ}
	GenerateCFrag {Targ}


MacOSROM								�	"{ImageDir}Mac OS ROM"

"{ImageDir}Mac OS ROM"					��	"{NewWorldDir}ZeroPad" �
											"{NewWorldDir}WrapMacOSROM" �
											"{NewWorldDir}BootScript.fs" �
											"{NewWorldDir}BootScriptVanilla.fs" �
											"{NewWorldDir}BootIcon" �
											"{NewWorldDir}BootBadge" �
											"{ImageDir}Trampoline.elf" �
											"{ImageDir}Parcels" �
											"{ObjDir}NQDResidentCursor" �
											"{ObjDir}ProcessMgrLib" �
											"{ToolDir}GenerateCFrag" "{ToolDir}ResRenumber" �
											"{NewWorldDir}ChecksumMacOSROM" "{ToolDir}Adler-32" �
											"{ToolDir}SubstituteEquates"
	"{NewWorldDir}WrapMacOSROM" �
		"{NewWorldDir}"BootScript`If "{FeatureSet}" !~ /�TbxiBetterScript=TRUE�/; Echo -n Vanilla; End`.fs �
		"{NewWorldDir}BootIcon" �
		"{NewWorldDir}BootBadge" �
		"{ImageDir}Trampoline.elf" �
		"{ImageDir}Parcels" �
		{Targ}
	MergeFragment -d -x "{ObjDir}NQDResidentCursor" "{ObjDir}ProcessMgrLib" {Targ}
	ResRenumber 0 cfrg 28 {Targ}
	"{NewWorldDir}ChecksumMacOSROM" {Targ}
	SetFile -c chrp -t tbxi -comments "`Echo {Features}`" {Targ}


"{ImageDir}Mac OS ROM"					��	"{PrebuiltDir}SystemEnabler:Mac OS ROM.r"
	Rez -append -c chrp -t tbxi -o {Targ} {ROptions} "{PrebuiltDir}SystemEnabler:Mac OS ROM.r"
	SetFile -comments "`Echo {Features}`" {Targ}


#_______________________________________________________________________
#	INCLUDES
#	(Some disabled)
#_______________________________________________________________________

# Tools.make and some actual tools were missing, so I wrote my own.

#include {ToolSrcDir}Tools.make

# The original build system gave no clue about building the upper megabyte of the
# 4 MB "RISC" ROM, which contains PowerPC specific code: the ConfigInfo struct,
# the Nanokernel and the 68k emulator. This hacks it together:

#include {RiscDir}RISC.make
#include {NKDir}NanoKernel.make

# Parcels.make, used by this makefile and BlueBox.make, is responsible for
# collecting the Mac OS ROM Parcel and others into a 'prcl' structure.

#include {NewWorldDir}Parcels.make

# This makefile wraps the above structure in a bootloader as a CHRP script,
# and also has to link in some code fragments that really belong to the
# file's resource fork. The resulting file is called "Mac OS ROM" and has
# the type/creator codes tbxi/chrp. On a NewWorld system, Open Firmware
# looks in a partition's System Folder for a 'tbxi' file and runs it.

#include {TrampolineDir}Trampoline.make

# Not in use:
##include {MakeDir}MainCode.make
##include {DeclDir}DeclData.make
##include {DriverDir}Drivers.make
##include {ResourceDir}Resources.make


#_______________________________________________________________________
#	CLEAN RULES
# 	(Other makefiles contain more granular clean rules, like CleanTools)
#_______________________________________________________________________

Clean							�
	Delete -i `Files -f -r -o -s "{BuildDir}"` � Dev:Null

CleanResources					�
	Delete -i {ResourceFiles} � Dev:Null
