/*
	File:		Parcels.r

	Contains:	Parcel descriptions which are compiled and used by ParcelLayout

	Change History (most recent first):

				  1/9/16	HQX		Updated with info from earlier Mac OS ROM versions
				  1/8/16	HQX		Painstakingly adapted from the output of a Python script
*/


#include "RomTypes.r"


/* The CodeRegister, CodePrepare, ROM and Checksum parcels are special, and go first */



resource 'prcl' (100, "CodePrepare") {
	kPropertiesForSpecialNode,
	{"CodePrepare Node Parcel"},
	0x00020000,
	{
		"name",
		0x00000000,
		useCstring {"AAPL,CodePrepare"};
		
		"AAPL,prepare_order",
		0x00000000,
		useCstringArray {{"Placeholder"}};

#if BlueBox
		kUseResName,
		0x00020094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "InterruptTreeBlueBoxLib"};
#else
		/* MotherBoardHAL was present in 6.1, absent in 7.5.1 */
		//kUseResName,
		//0x00020094,
		//fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "MotherBoardHAL"};
		
		kUseResName,
		0x00020094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "TimeManagerLib"};
		
		kUseResName,
		0x00020094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "NVRAMServicesLib"};
		
		kUseResName,
		0x00020094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "RTCServicesLib"};
#endif
	};
};


resource 'prcl' (110, "CodeRegister") {
	kPropertiesForSpecialNode,
	{"CodeRegister Node Parcel"},
	0x00010000,
	{
		"name",
		0x00000000,
		useCstring {"AAPL,CodeRegister"};
		
		kUseResName,
		0x00010094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "NativePowerMgrLib"};

#if !BlueBox
		/* AGPLib was absent in 7.5.1, present in 8.4 */
		kUseResName,
		0x00010094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "AGPLib"};
		
		/* EtherPrintfLib AND StartLib were absent in 3.8 and earlier, present in 6.1 */
		kUseResName,
		0x00010194,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'nlib', "EtherPrintfLib"};
#endif
		
		kUseResName,
		0x00010094,
		fromResource {RsrcDir, "MiscLibs.rsrc", 'shlb', "StartLib"};
	};
};


/* Naturally the most important parcel */

resource 'prcl' (200, "The Really Important Parcel") {
	'rom ',
	{"Mac OS ROM Parcel"},
	0x00000000,
	{
		"",
		0x00000005,
		fromDataFork {ImageDir, "RiscMondo"};
	};
};


/* I wonder what these calculated checksums are used for... */

resource 'prcl' (300, "Property Checksum") {
	'psum',
	{"Property Checksum"},
	0x00000000,
	{
		"",
		0x00000000,
		useCstringArray {{
			"name",
			"model",
			"compatible",
			"device_type",
			"reg",
			"assigned-addresses",
			"slot-names",
			"vendor-id",
			"device-id",
			"class-code",
			"devsel-speed",
			"fast-back-to-back",
			"bootpath",
			"mac-address",
		}};
		
		"",
		0x00000000,
		useCstringArray {{
			"boot-rom",
			"macos",
		}};
		
		"",
		0x00000000,
		useCstringArray {{
			"Placeholder",
		}};
		
		"",
		0x00000000,
		useCstringArray {{
			"Placeholder",
		}};
		
		"",
		0x00000000,
		useCstringArray {{
			"usb",
			"ieee1394",
		}};
	};
};


/* Now the 'prop' parcels, which are used to modify actual devices in the tree */
/* (in alphabetical order) */

/* BlueBox uses:	a "bal" (BlueAbstractionLayer) parcel
					a MacOSROMFile-version parcel
					all parcels containing a PowerMgrPlugin
*/


#if !BlueBox
resource 'prcl' (1000, "Network: apple21143") {
	kPropertiesForExistingNode,
	{"apple21143", "network"},
	0x0000000c,
	{
		"lanLib,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "Network.rsrc", 'shlb', "apple21143"};
	};
};
#endif


/* backlight driver was absent in 3.8, present in 6.1 */
#if !BlueBox
resource 'prcl' (1010, "Backlight") {
	kPropertiesForExistingNode,
	{"backlight", "backlight"},
	0x00000009,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000000,

#if CusterBacklightParcel
		fromResource {RsrcDir, "Display.rsrc", 'ndrv', "CusterBacklightDriver"};
#else
		fromResource {RsrcDir, "Display.rsrc", 'ndrv', "BacklightDriver"};
#endif
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"BacklightDriver"};
	};
};
#endif


#if BlueBox
resource 'prcl' (1015, "BAL PowerMgrPlugin") {
	kPropertiesForExistingNode,
	{"bal", "power-mgmt"},
	0x00000009,
	{
		"pef,AAPL,MacOS,PowerPC,register",
		0x00000000,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'ndrv', "bal"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"PowerMgrPlugin"};
	};
};
#endif


/* bmac+ driver was absent in 7.5.1, present in 8.4 */
#if !BlueBox
resource 'prcl' (1020, "Network: bmac+") {
	kPropertiesForExistingNode,
	{"bmac+", "network"},
	0x0000000c,
	{
		"lanLib,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "Network.rsrc", 'shlb', "bmac+"};
	};
};
#endif


/* PCCard library was in "cardbus"-named parcel in 3.8,
/* moved down to "cardbus"-compat parcel in 6.1 */

#if !BlueBox
//resource 'prcl' (1025, "Old CardBus Parcel") {
//	kPropertiesForExistingNode,
//	{"cardbus", "cardbus"},
//	0x00000009,
//	{
//		kUseResName,
//		0x00010094,
//		fromResource {RsrcDir, "PCCard.rsrc", 'nlib', "PCCard"};
//	};
//};
#endif


/* chrp-named parcels were present in 3.8, absent in 6.1 */

#if !BlueBox
//resource 'prcl' (1026, "CHRP parallel?") {
//	kPropertiesForExistingNode,
//	{"chrp,ecp", "parallel"},
//	0x0000000c,
//	{
//		"driver,AAPL,MacOS,PowerPC",
//		0x00000006,
//		fromResource {RsrcDir, "LegacyDrivers.rsrc", 'ndrv', "parallel"};
//	};
//};
#endif


#if !BlueBox
//resource 'prcl' (1027, "chrp,ide/ide") {
//	kPropertiesForExistingNode,
//	0x0000000c,
//	{
//		"driver,AAPL,MacOS,PowerPC",
//		0x00000006,
//		fromResource {RsrcDir, "LegacyDrivers.rsrc", 'ndrv', "ide"};
//	};
//};
#endif


#if !BlueBox
resource 'prcl' (1030) {
	kPropertiesForExistingNode,
	{"cmd646-ata", "ata"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "ATA.rsrc", 'ndrv', "cmd646-ata"};
	};
};
#endif


/* cofb driver was absent in 3.8, present in 6.1 */
#if !BlueBox
resource 'prcl' (1040, "Framebuffer... generic?") {
	kPropertiesForExistingNode,
	{"cofb", "display"},
	0x00000008,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000024,
		fromResource {RsrcDir, "Display.rsrc", 'ndrv', "cofb"};
	};
};
#endif


/* Present in both BlueBox and bare metal */
resource 'prcl' (1050, "CUDA PowerMgrPlugin") {
	kPropertiesForExistingNode,
	{"cuda", "via-cuda"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,register",
		0x00000000,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'ndrv', "cuda"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"PowerMgrPlugin"};
	};
};


#if !BlueBox
resource 'prcl' (1060, "Network: bmac+") {
	kPropertiesForExistingNode,
	{"gmac", "network"},
	0x0000000c,
	{
		"lanLib,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "Network.rsrc", 'shlb', "gmac"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1070, "Grackle... the G3 PCI controller?") {
	kPropertiesForExistingNode,
	{"grackle","pci"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,prepare",
		0x00000016,
		fromResource {RsrcDir, "PCI.rsrc", 'nlib', "Grackle_PCICyclesLib"};
		
		"code,AAPL,MacOS,name",
		0x00000000,
		useCstring {"Grackle_PCICyclesLib"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1080, "ATA: Heathrow") {
	kPropertiesForExistingNode,
	{"heathrow-ata", "ide"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "ATA.rsrc", 'ndrv', "heathrow-ata"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1090, "ATA: Heathrow again!") {
	kPropertiesForExistingNode,
	{"heathrow-ata", "ata"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000002,
		fromResource {RsrcDir, "ATA.rsrc", 'ndrv', "heathrow-ata"};
	};
};
#endif


/* The Kauai driver is present in 9.1.1 and later, absent in 9.0.1 and earlier */
#if !BlueBox
resource 'prcl' (1100, "ATA: Kauai") {
	kPropertiesForExistingNode,
	{"kauai-ata", "ata"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "ATA.rsrc", 'ndrv', "kauai-ata"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1110, "ATA: KeyLargo") {
	kPropertiesForExistingNode,
	{"keylargo-ata", "ata"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "ATA.rsrc", 'ndrv', "keylargo-ata"};
	};
};
#endif


/* The I2C driver is present in 9.6.1 and later, absent in 9.1.1 and earlier */
/* (See also uni-n-i2c below) */
#if !BlueBox
resource 'prcl' (1120, "I2C on KeyWest") {
	kPropertiesForExistingNode,
	{"keywest-i2c", "i2c"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "I2C.rsrc", 'ndrv', "i2c"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1130, "NVRAM on MacIO") {
	kPropertiesForExistingNode,
	{"mac-io", "nvram"},
	0x0000000a,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "NVRAM.rsrc", 'ndrv', "mac-io"};
	};
};
#endif


resource 'prcl' (1140, "File version") {
	kPropertiesForExistingNode,
	{"macos"},
	0x00000001,
	{
		"MacOSROMFile-version",
		0x00000000,

#if BlueBox
		useCstring {"4.8GMc1"};
#else
		useCstring {"10.2f1"};
#endif

	};
};


#if !BlueBox
resource 'prcl' (1150, "'flash' NVRAM??") {
	kPropertiesForExistingNode,
	{"nvram,flash", "nvram"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "NVRAM.rsrc", 'ndrv', "flash"};
	};
};
#endif


/* PCCard library was in a parcel called "cardbus" in 3.8 and earlier, moved here in 6.1 */
/* These two CardBus drivers were separate in 7.5.1 and earlier, merged in 8.4 */
#if !BlueBox
resource 'prcl' (1160, "CardBus 1") {
	kPropertiesForExistingNode,
	{"pci104c,ac1a", "cardbus"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000016, /* 0x14 in 7.5.1 and earlier; like this in 8.4 */
		fromResource {RsrcDir, "PCCard.rsrc", 'ndrv', "cardbus"};
		
		"PCCard",
		0x00010094,
		fromResource {RsrcDir, "PCCard.rsrc", 'nlib', "PCCard"};
	};
};
#endif


/* This second CardBus parcel was absent in 6.1 and earlier, present in 7.5.1 */
#if !BlueBox
resource 'prcl' (1170, "CardBus 2") {
	kPropertiesForExistingNode,
	{"pci104c,ac50", "cardbus"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000016,
		fromResource {RsrcDir, "PCCard.rsrc", 'ndrv', "cardbus"};
		
		"PCCard",
		0x00010094,
		fromResource {RsrcDir, "PCCard.rsrc", 'nlib', "PCCard"};
	};
};
#endif


/* FireWire parcel absent in 3.0, present in 3.7 */
#if !BlueBox
resource 'prcl' (1180, "FireWire") {
	kPropertiesForExistingNode,
	{"pciclass,0c0010", "ieee1394"},
	0x0000020c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "FireWire.rsrc", 'ndrv', "firewire"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "FireWire.rsrc", 'nlib', "FWServicesLib"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "FireWire.rsrc", 'nlib', "DevNLib"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "FireWire.rsrc", 'nlib', "DFMLib"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "FireWire.rsrc", 'nlib', "GenericDriverFamily"};
	};
};
#endif


resource 'prcl' (1190) {
	kPropertiesForExistingNode,
	{"pmu", "power-mgt"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,register",
		0x00000000,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'ndrv', "pmu"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"PowerMgrPlugin"};
		
		"target-mode-capable",
		0x00000002,
		useCstring {"SCSI"};
		
		/* This came before the one above in 3.8, in this position in 6.1. Wonder why... */
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'nlib', "PMULib"};
	};
};


/* The I2C driver is present in 9.6.1 and later, absent in 9.1.1 and earlier */
/* (See also keywest-i2c above) */
#if !BlueBox
resource 'prcl' (1200, "I2C on UniNorth") {
	kPropertiesForExistingNode,
	{"uni-n-i2c", "i2c"},
	0x0000000c,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000006,
		fromResource {RsrcDir, "I2C.rsrc", 'ndrv', "i2c"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1210) {
	kPropertiesForExistingNode,
	{"uni-north", "pci"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,prepare",
		0x00000016,
		fromResource {RsrcDir, "PCI.rsrc", 'nlib', "UniNorth_PCICyclesLib"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"UniNorth_PCICyclesLib"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1220, "RTC on CUDA") {
	kPropertiesForExistingNode,
	{"via-cuda", "rtc"},
	0x0000000a,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "RTC.rsrc", 'ndrv', "cuda"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1230, "NVRAM on PMU") {
	kPropertiesForExistingNode,
	{"via-pmu", "nvram"},
	0x0000000a,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "NVRAM.rsrc", 'ndrv', "pmu"};
	};
};
#endif


#if !BlueBox
resource 'prcl' (1240, "RTC on PMU") {
	kPropertiesForExistingNode,
	{"via-pmu", "rtc"},
	0x0000000a,
	{
		"driver,AAPL,MacOS,PowerPC",
		0x00000004,
		fromResource {RsrcDir, "RTC.rsrc", 'ndrv', "pmu"};
	};
};
#endif


/* The PMU2000 parcel was absent in 6.1 and earlier, present in 7.5.1 */
resource 'prcl' (1250) {
	kPropertiesForExistingNode,
	{"via-pmu-2000", "power-mgt"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,register",
		0x00000000,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'ndrv', "pmu-2000"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"PowerMgrPlugin"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'nlib', "PMULib"};
	};
};

resource 'prcl' (1260) {
	kPropertiesForExistingNode,
	{"via-pmu-99", "power-mgt"},
	0x0000000c,
	{
		"pef,AAPL,MacOS,PowerPC,register",
		0x00000000,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'ndrv', "pmu-99"};
		
		"code,AAPL,MacOS,name",
		0x00000002,
		useCstring {"PowerMgrPlugin"};
		
		kUseResName,
		0x00010096,
		fromResource {RsrcDir, "PowerManagement.rsrc", 'nlib', "PMULib"};
	};
};
