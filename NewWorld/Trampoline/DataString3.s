	STRING   C

	

	; Using the native assembler's ALIGN directive puts nops everywhere,

	; but that doesn't matter between these cstrings (I hope)

	

	DC.B 'escc-legacy'													; 0x4340

	ALIGN 3

	DC.B 'chrp,es1'														; 0x4350

	ALIGN 3

	DC.B 'escc-legacy'													; 0x4360

	ALIGN 3

	DC.B 'ch-a'															; 0x4370

	ALIGN 3

	DC.B 'scc-audio-control'											; 0x4378

	ALIGN 3

	DC.B 'serial'														; 0x4390

	ALIGN 3

	DC.B 'ch-a'															; 0x4398

	ALIGN 3

	DC.B 'chrp,es2'														; 0x43a0

	ALIGN 3

	DC.B 'serial'														; 0x43b0

	ALIGN 3

	DC.B 'ch-b'															; 0x43b8

	ALIGN 3

	DC.B 'chrp,es3'														; 0x43c0

	ALIGN 3

	DC.B 'serial'														; 0x43d0

	ALIGN 3

	DC.B 'ch-a'															; 0x43d8

	ALIGN 3

	DC.B 'chrp,es4'														; 0x43e0

	ALIGN 3

	DC.B 'serial'														; 0x43f0

	ALIGN 3

	DC.B 'ch-b'															; 0x43f8

	ALIGN 3

	DC.B 'chrp,es5'														; 0x4400

	ALIGN 3

	DC.B 'serial'														; 0x4410

	ALIGN 3

	DC.B 'adb'															; 0x4418

	ALIGN 3

	DC.B 'chrp,adb0'													; 0x4420

	ALIGN 3

	DC.B 'adb'															; 0x4430

	ALIGN 3

	DC.B 'adb'															; 0x4438

	ALIGN 3

	DC.B 'pmu'															; 0x4440

	ALIGN 3

	DC.B 'adb'															; 0x4448

	ALIGN 3

	DC.B 'adb'															; 0x4450

	ALIGN 3

	DC.B 'pmu-99'														; 0x4458

	ALIGN 3

	DC.B 'adb'															; 0x4460

	ALIGN 3

	DC.B 'adb'															; 0x4468

	ALIGN 3

	DC.B 'adb'															; 0x4470

	ALIGN 3

	DC.B 'keyboard'														; 0x4478

	ALIGN 3

	DC.B 'keyboard'														; 0x4488

	ALIGN 3

	DC.B 'via'															; 0x4498

	ALIGN 3

	DC.B 'via'															; 0x44a0

	ALIGN 3

	DC.B 'via-cuda'														; 0x44a8

	ALIGN 3

	DC.B 'via-cuda'														; 0x44b8

	ALIGN 3

	DC.B 'via-pmu'														; 0x44c8

	ALIGN 3

	DC.B 'via-pmu'														; 0x44d0

	ALIGN 3

	DC.B 'ethernet'														; 0x44d8

	ALIGN 3

	DC.B 'scsi'															; 0x44e8

	ALIGN 3

	DC.B 'chrp,mesh0'													; 0x44f0

	ALIGN 3

	DC.B 'scsi'															; 0x4500

	ALIGN 3

	DC.B 'interrupt-controller'											; 0x4508

	ALIGN 3

	DC.B 'chrp,open-pic'												; 0x4520

	ALIGN 3

	DC.B 'open-pic'														; 0x4530

	ALIGN 3

	DC.B 'mac-io'														; 0x4540

	ALIGN 3

	DC.B 'mac-io'														; 0x4548

	ALIGN 3

	DC.B 'ext-mac-io'													; 0x4550

	ALIGN 3

	DC.B 'mac-io'														; 0x4560

	ALIGN 3

	DC.B 'starlifter-bridge'											; 0x4568

	ALIGN 3

	DC.B 'pci-bridge'													; 0x4580

	ALIGN 3

	DC.B 'pci'															; 0x4590

	ALIGN 3

	DC.B 'heathrow-ata'													; 0x4598

	ALIGN 3

	DC.B 'keylargo-ata'													; 0x45a8

	ALIGN 3

	DC.B 'interrupt-controller'											; 0x45b8

	ALIGN 3

	DC.B 'heathrow'														; 0x45d0

	ALIGN 3

	DC.B 'interrupt-controller'											; 0x45e0

	ALIGN 3

	DC.B 'interrupt-controller'											; 0x45f8

	ALIGN 3

	DC.B 'paddington'													; 0x4610

	ALIGN 3

	DC.B 'interrupt-controller'											; 0x4620

	ALIGN 3

	DC.B 'programmer-switch'											; 0x4638

	ALIGN 3

	DC.B 'fdc'															; 0x4650

	ALIGN 3

	DC.B 'swim3'														; 0x4658

	ALIGN 3

	DC.B 'sound'														; 0x4660

	ALIGN 3

	DC.B 'awacs'														; 0x4668

	ALIGN 3

	DC.B 'sound'														; 0x4670

	ALIGN 3

	DC.B 'davbus'														; 0x4678

	ALIGN 3

	DC.B 'power-mgt'													; 0x4680

	ALIGN 3

	DC.B 'cuda'															; 0x4690

	ALIGN 3

	DC.B 'power-mgt'													; 0x4698

	ALIGN 3

	DC.B 'power-mgt'													; 0x46a8

	ALIGN 3

	DC.B 'pmu'															; 0x46b8

	ALIGN 3

	DC.B 'power-mgt'													; 0x46c0

	ALIGN 3

	DC.B 'power-mgt'													; 0x46d0

	ALIGN 3

	DC.B 'via-pmu-99'													; 0x46e0

	ALIGN 3

	DC.B 'power-mgt'													; 0x46f0

	ALIGN 3

	DC.B 'pccard'														; 0x4700

	ALIGN 3

	DC.B 'ti1130'														; 0x4708

	ALIGN 3

	DC.B 'pccard'														; 0x4710

	ALIGN 3

	DC.B 'cardbus'														; 0x4718

	ALIGN 3

	DC.B 'ti1210'														; 0x4720

	ALIGN 3

	DC.B 'cardbus'														; 0x4728

	ALIGN 3

	DC.B 'cardbus'														; 0x4730

	ALIGN 3

	DC.B 'pci104c,ac50'													; 0x4738

	ALIGN 3

	DC.B 'cardbus'														; 0x4748

	ALIGN 3

	DC.B 'usb'															; 0x4750

	ALIGN 3

	DC.B 'usb'															; 0x4758

	ALIGN 3

	DC.B 'backlight'													; 0x4760

	ALIGN 3

	DC.B 'mouse'														; 0x4770

	ALIGN 3

	DC.B 'mouse'														; 0x4778

	ALIGN 3

	DC.B 'keylargo-timer'												; 0x4780

	ALIGN 3

	