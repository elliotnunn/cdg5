	STRING   AsIs
	
	DC.B '******************* MacOS: Fatal Error!  (0xF3B37FDB) *******************', 0x0d, 0
																		; 0x0698
	
	DC.B 'MacOS: Boot Failure!  (0xF3C481F6)', 0x0d, 0					; 0x06e3
	DC.B 'exit', 0														; 0x0707
	DC.B 'open', 0														; 0x070c
	DC.B 'close', 0														; 0x0711
	DC.B 'read', 0														; 0x0717
	DC.B 'seek', 0														; 0x071c
	DC.B 'finddevice', 0												; 0x0721
	DC.B '/aliases', 0													; 0x072c
	DC.B 'getproplen', 0												; 0x0735
	DC.B 'keyboard', 0													; 0x0740
	DC.B 'getprop', 0													; 0x0749
	DC.B 'get-key-map', 0												; 0x0751
	
	DC.B 'IsKeyDown: keycode[0x%x] is down in keymap[%d] (0x%x)', 0x0d, 0
																		; 0x075d
	
	DC.B 'IsKeyDown: no keys held down', 0x0d, 0						; 0x0794
	DC.B 'key?', 0														; 0x07b2
	DC.B 'key', 0														; 0x07b7
	DC.B 'set-colors', 0												; 0x07bb
	DC.B 'fill-rectangle', 0											; 0x07c6
	DC.B 'draw-rectangle', 0											; 0x07d5
	DC.B '/', 0															; 0x07e4
	
	DC.B 'Unable to get phandle to "/".  Device tree probably corrupt.', 0x0d, 0
																		; 0x07e6
	
	DC.B 'color-code', 0												; 0x0824
	DC.B 'screen', 0													; 0x082f
	DC.B 'dimensions', 0												; 0x0836
	DC.B 'setprop', 0													; 0x0841
	DC.B '\\\\:bear', 0													; 0x0849
	DC.B 'package-to-path', 0											; 0x0851
	DC.B 'set-hybernot-flag', 0											; 0x0861
	DC.B 'instance-to-package', 0										; 0x0873
	DC.B 'hybernot', 0													; 0x0887
	DC.B 'network', 0													; 0x0890
	DC.B 'nvram', 0														; 0x0898
	DC.B '/nvram', 0													; 0x089e
	
	DC.B 'MacOS: unable to find ihandle to NVRAM in "/chosen"; trying "%s".', 0x0d, 0
																		; 0x08a5
	
	DC.B 'instance-to-path', 0											; 0x08e8
	DC.B 'NVRAM path: %s; result = %d', 0x0d, 0							; 0x08f9
	
	DC.B 'MacOS: unable to acquire ihandle for NVRAM node "%s" - using offset of 0x1400', 0x0d, 0
																		; 0x0916
	
	DC.B 'size', 0														; 0x0965
	DC.B 'MacOS: NVRAM "size" method failure - using 8K.', 0x0d, 0		; 0x096a
	DC.B 'NVRAM size = 0x%08X.', 0x0d, 0								; 0x099a
	
	DC.B 'NVRAM partition offset=%08X, SIG=%02X, size=%04X, name=''%12s''', 0x0d, 0
																		; 0x09b0
	
	DC.B 'NVRAM partition invalid (bad checksum).', 0x0d, 0				; 0x09ef
	DC.B 'APL,MacOS75', 0												; 0x0a18
	
	DC.B 'MacOS: NVRAM size too small - using constant NVRAM offset = 0x1400.', 0x0d, 0
																		; 0x0a24
	
	DC.B 'MacOS: unable to find a usable NVRAM partition - using offset 0x1400.', 0x0d, 0
																		; 0x0a69
	
	DC.B 'write', 0														; 0x0ab0
	DC.B 0																; 0x0ab6
	DC.B 'MacOS NVRAM partition offset = 0x%x', 0x0d, 0					; 0x0ab7
	DC.B '/pci/isa/nvram', 0											; 0x0adc
	DC.B 'Cannot find NVRAM node, defaulting to %s', 0x0d, 0			; 0x0aeb
	DC.B 'Found NVRAM node: %s', 0x0d, 0								; 0x0b15
	DC.B 'Could not get NVRAM instance Handle', 0x0d, 0					; 0x0b2b
	DC.B 'RAM Disk Size: %d', 0x0d, 0									; 0x0b50
	DC.B 'DR Emulator Cache Size: %d', 0x0d, 0							; 0x0b63
	DC.B 'Updated BusClockRateHz: %d', 0x0d, 0							; 0x0b7f
	
	DC.B 'Overlap in AddAddrRangeRecord() - start = 0x%08X, length = 0x%08X, entry->start = 0x%08X, entry->length = 0x%08X.', 0x0d, 0
																		; 0x0b9b
	
	DC.B 'Overlap in AddMemoryRelocationEntry().', 0x0d, 0				; 0x0c0e
	
	DC.B '    - dst = 0x%08X, dstLength = 0x%08X, entry->dst = 0x%08X, entry->dst_len = 0x%08X.', 0x0d, 0
																		; 0x0c36
	
	DC.B '    - src = 0x%08X, srcLength = 0x%08X, entry->src = 0x%08X, entry->src_len = 0x%08X.', 0x0d, 0
																		; 0x0c8d
	
	DC.B 'Can''t add exception vector address range entry', 0x0d, 0		; 0x0ce4
	DC.B 'MacOS: physicalStart not page-aligned.', 0x0d, 0				; 0x0d14
	DC.B 'MacOS: length not page-aligned.', 0x0d, 0						; 0x0d3c
	
	DC.B 'MacOS: Unable to allocate resume buffer. 0x%0x%08X bytes.', 0x0d, 0
																		; 0x0d5d
	
	DC.B 'MacOS: Unable to determine resume buffer''s physical address.', 0x0d, 0
																		; 0x0d98
	
	DC.B 'MacOS: Unable to read resume data.', 0x0d, 0					; 0x0dd6
	
	DC.B 'MacOS: Unable to add resume buffer to memory relocation list!', 0x0d, 0
																		; 0x0dfa
	
	DC.B 'MacOS: Unable to allocate temp range buffer.', 0x0d, 0		; 0x0e39
	DC.B 'Can''t add memoryrelocation address range entry', 0x0d, 0		; 0x0e67
	DC.B 'MacOS: Unable to allocate relocationEngineParams.', 0x0d, 0	; 0x0e97
	
	DC.B 'Can''t add relocation engine params address range entry', 0x0d, 0
																		; 0x0eca
	
	DC.B 'MacOS: Cannot find space for RelocationEngine.', 0x0d, 0		; 0x0f02
	DC.B 'MacOS: unable to locate "/memory" node.', 0x0d, 0				; 0x0f32
	DC.B 'reg', 0														; 0x0f5b
	DC.B 'MacOS: no physical memory detected!', 0x0d, 0					; 0x0f5f
	DC.B 'mmu', 0														; 0x0f84
	DC.B 'flush-on-lock', 0												; 0x0f88
	DC.B '/rtas', 0														; 0x0f96
	DC.B 'MacOS: RTAS not found.', 0x0d, 0								; 0x0f9c
	DC.B 'rtas-size', 0													; 0x0fb4
	DC.B 'claim', 0														; 0x0fbe
	
	DC.B 'MacOS: unable to claim memory for RTAS instantiation!', 0x0d, 0
																		; 0x0fc4
	
	DC.B 'MacOS: found RTAS node, but rtas-size is zero.', 0x0d, 0		; 0x0ffb
	DC.B 'MacOS: RTAS -open- method not found or failed', 0x0d, 0		; 0x102b
	DC.B 'instantiate-rtas', 0											; 0x105a
	DC.B 'MacOS: instantiate-rtas failed.', 0x0d, 0						; 0x106b
	DC.B 'MacOS: "/rtas" not found!', 0x0d, 0							; 0x108c
	DC.B 'nvram-fetch', 0												; 0x10a7
	DC.B 'MacOS: RTAS nvram-fetch token not found', 0x0d, 0				; 0x10b3
	DC.B 'nvram-store', 0												; 0x10dc
	DC.B 'MacOS: RTAS nvram-store token not found', 0x0d, 0				; 0x10e8
	DC.B 'get-time-of-day', 0											; 0x1111
	DC.B 'MacOS: RTAS get-time-of-day token not found', 0x0d, 0			; 0x1121
	DC.B 'set-time-of-day', 0											; 0x114e
	DC.B 'MacOS: RTAS set-time-of-day token not found', 0x0d, 0			; 0x115e
	DC.B 'system-reboot', 0												; 0x118b
	DC.B 'MacOS: RTAS system-reboot token not found', 0x0d, 0			; 0x1199
	DC.B 'power-off', 0													; 0x11c4
	DC.B 'MacOS: RTAS power-off token not found', 0x0d, 0				; 0x11ce
	DC.B 'set-time-for-power-on', 0										; 0x11f5
	DC.B 'get-time-for-power-on', 0										; 0x120b
	DC.B 'event-scan', 0												; 0x1221
	DC.B 'check-exception', 0											; 0x122c
	DC.B 'read-pci-config', 0											; 0x123c
	DC.B 'MacOS: RTAS read-pci-config token not found', 0x0d, 0			; 0x124c
	DC.B 'write-pci-config', 0											; 0x1279
	DC.B 'MacOS: RTAS write-pci-config token not found', 0x0d, 0		; 0x128a
	DC.B 'MacOS: Cannot allocate vectorMaskTable.', 0x0d, 0				; 0x12b8
	DC.B 'MacOS: Cannot allocate cascadeInfo.', 0x0d, 0					; 0x12e1
	DC.B 0x0d, 'Interrupt masks:', 0x0d, 0								; 0x1306
	DC.B '  Level         Raw Value  Bits active', 0x0d, 0				; 0x1319
	DC.B '    %d [00..31]  %08X   ', 0									; 0x1341
	DC.B '      [32..63]  %08X   ', 0									; 0x135a
	DC.B 0x0d, 0														; 0x1372
	DC.B '%d ', 0														; 0x1374
	DC.B 'MacOS: Cannot allocate interruptVectorLookupTable.', 0x0d, 0	; 0x1378
	DC.B 'MacOS: Cannot allocate gInterruptPriorityTable.', 0x0d, 0		; 0x13ac
	DC.B 'Interrupt vectors:', 0x0d, 0									; 0x13dd
	DC.B 'Interrupt priorities:', 0x0d, 0								; 0x13f1
	DC.B 'Spurious interrupt vector = 0x%X', 0x0d, 0					; 0x1408
	DC.B 'SCSIIntVect = %d', 0x0d, 0									; 0x142a
	DC.B 'SCCAIntVect = %d', 0x0d, 0									; 0x143c
	DC.B 'SCCBIntVect = %d', 0x0d, 0									; 0x144e
	DC.B 'VIAIntVect  = %d', 0x0d, 0									; 0x1460
	DC.B 'ADBIntVect  = %d', 0x0d, 0									; 0x1472
	DC.B 'NMIIntVect  = %d', 0x0d, 0									; 0x1484
	DC.B 'Heathrow base address = 0x%X, phys = 0x%08X', 0x0d, 0			; 0x1496
	DC.B '/cpus/@0', 0													; 0x14c3
	DC.B 'MacOS: unable to locate "/cpus/@0"', 0x0d, 0					; 0x14cc
	DC.B '603-translation', 0											; 0x14f0
	DC.B 'cpu-version', 0												; 0x1500
	DC.B 'MacOS: missing cpu "cpu-version" property.', 0x0d, 0			; 0x150c
	DC.B 'clock-frequency', 0											; 0x1538
	DC.B 'MacOS: missing cpu "clock-frequency" property.', 0x0d, 0		; 0x1548
	DC.B 'bus-frequency', 0												; 0x1578
	DC.B 'MacOS: missing cpu "bus-frequency" property.', 0x0d, 0		; 0x1586
	DC.B 'timebase-frequency', 0										; 0x15b4
	DC.B 'MacOS: missing cpu "timebase-frequency" property.', 0x0d, 0	; 0x15c7
	DC.B 'd-cache-size', 0												; 0x15fa
	DC.B 'MacOS: missing cpu "d-cache-size" property.', 0x0d, 0			; 0x1607
	DC.B 'i-cache-size', 0												; 0x1634
	DC.B 'MacOS: missing cpu "i-cache-size" property.', 0x0d, 0			; 0x1641
	DC.B 'reservation-granule-size', 0									; 0x166e
	DC.B 'reservation-granularity', 0									; 0x1687
	
	DC.B 'MacOS: missing cpu "reservation-granule-size" property.', 0x0d, 0
																		; 0x169f
	
	DC.B 'cache-unified', 0												; 0x16d8
	DC.B 'd-cache-block-size', 0										; 0x16e6
	DC.B 'MacOS: missing cpu "d-cache-block-size" property.', 0x0d, 0	; 0x16f9
	DC.B 'i-cache-block-size', 0										; 0x172c
	DC.B 'MacOS: missing cpu "i-cache-block-size" property.', 0x0d, 0	; 0x173f
	DC.B 'd-cache-line-size', 0											; 0x1772
	DC.B 'i-cache-line-size', 0											; 0x1784
	DC.B 'd-cache-sets', 0												; 0x1796
	DC.B 'MacOS: missing cpu "d-cache-sets" property.', 0x0d, 0			; 0x17a3
	DC.B 'i-cache-sets', 0												; 0x17d0
	DC.B 'MacOS: missing cpu "i-cache-sets" property.', 0x0d, 0			; 0x17dd
	DC.B 'tlb-size', 0													; 0x180a
	DC.B 'MacOS: missing cpu "tlb-size" property.', 0x0d, 0				; 0x1813
	DC.B 'tlb-sets', 0													; 0x183c
	DC.B 'MacOS: missing cpu "tlb-sets" property.', 0x0d, 0				; 0x1845
	DC.B '/cpus/@0/l2-cache', 0											; 0x186e
	
	DC.B 'MacOS: missing "/cpus/l2-cache" "d-cache-size" property.', 0x0d, 0
																		; 0x1880
	
	DC.B 'MacOS: missing "/cpus/l2-cache" "i-cache-size" property.', 0x0d, 0
																		; 0x18ba
	
	DC.B 'MacOS: missing "/cpus/l2-cache" "d-cache-sets" property.', 0x0d, 0
																		; 0x18f4
	
	DC.B 'MacOS: missing "/cpus/l2-cache" "i-cache-sets" property.', 0x0d, 0
																		; 0x192e
	
	DC.B '/cpus/@0/l2-cache/l2-cache', 0								; 0x1968
	
	DC.B 'MacOS: missing "/cpus/l2-cache/l2-cache" "d-cache-size" property.', 0x0d, 0
																		; 0x1983
	
	DC.B 'MacOS: missing "/cpus/l2-cache/l2-cache" "i-cache-size" property.', 0x0d, 0
																		; 0x19c6
	
	DC.B 'MacOS:  There are 26 banks of memory.  In copying ROM image, we could lose the last bank!', 0x0d, 0
																		; 0x1a09
	
	DC.B 'Can''t add toolbox rom memory relocation entry one!', 0x0d, 0	; 0x1a64
	DC.B 'Can''t add toolbox rom memory relocation entry two!', 0x0d, 0	; 0x1a98
	DC.B 'dev ', 0														; 0x1acc
	DC.B ' '' open', 0													; 0x1ad1
	DC.B 'ApplyATAPatch:  patch applied for code at 0x%x', 0x0d, 0		; 0x1ad9
	DC.B 'test', 0														; 0x1b09
	DC.B 'interpret', 0													; 0x1b0e
	DC.B 'CheckSwitchBoot: ''interpret'' not implemented', 0x0d, 0		; 0x1b18
	
	DC.B 'CheckSwitchBoot: no partition number specified in bootpath, will not switch boot', 0x0d, 0
																		; 0x1b46
	
	DC.B '0', 0															; 0x1b98
	DC.B 'CheckSwitchBoot: open of ''%s'' failed', 0x0d, 0				; 0x1b9a
	DC.B 'block-size', 0												; 0x1bc0
	
	DC.B 'CheckSwitchBoot: block-size method not implemented - not switch booting', 0x0d, 0
																		; 0x1bcb
	
	DC.B 'CheckSwitchBoot: block size is %d (greater than 512) - not switch booting', 0x0d, 0
																		; 0x1c14
	
	DC.B 'read-blocks', 0												; 0x1c5f
	DC.B 'CheckSwitchBoot: read-blocks method not implemented', 0x0d, 0	; 0x1c6b
	
	DC.B 'CheckSwitchBoot: read of driverDescriptorRecord failed', 0x0d, 0
																		; 0x1ca0
	
	DC.B 'CheckSwitchBoot: invalid driverDescriptorRecord signature', 0x0d, 0
																		; 0x1cd8
	
	DC.B 'CheckSwitchBoot: read of partition map entry failed', 0x0d, 0	; 0x1d13
	
	DC.B 'CheckSwitchBoot: invalid partition map entry signature', 0x0d, 0
																		; 0x1d48
	
	DC.B 'CheckSwitchBoot: read of bootPartitionPath failed', 0x0d, 0	; 0x1d80
	
	DC.B 'CheckSwitchBoot: HFS MDB signature incorrect.  Expected 0x%X, got 0x%X', 0x0d, 0
																		; 0x1db3
	
	DC.B 'drAlBlkSiz = %d, startBlock = %d, drAlBlSt = %d', 0x0d, 0		; 0x1dfb
	DC.B 'hfsplus MDB offset.hi = 0x%x, offset.lo = 0x%x', 0x0d, 0		; 0x1e2c
	
	DC.B 'CheckSwitchBoot: read of HFS Plus volume header failed', 0x0d, 0
																		; 0x1e5c
	
	DC.B 'CheckSwitchBoot: HFS+ volume header signature incorrect.  Expected 0x%X, got 0x%X', 0x0d, 0
																		; 0x1e94
	
	DC.B 'Blessed DirID = 0x%x', 0x0d, 0								; 0x1ee7
	DC.B 'OS 9 DirID = 0x%x', 0x0d, 0									; 0x1efd
	DC.B 'OS X DirID = 0x%x', 0x0d, 0									; 0x1f10
	
	DC.B 'CheckSwitchBoot: No alternate OS DirID set, cannot switch boot', 0x0d, 0
																		; 0x1f23
	
	DC.B 'write-blocks', 0												; 0x1f63
	DC.B 'CheckSwitchBoot: write-blocks method not implemented', 0x0d, 0; 0x1f70
	DC.B 'CheckSwitchBoot: write of new blessed DirID failed', 0x0d, 0	; 0x1fa6
	
	DC.B 'CheckSwitchBoot: dirID already set to OS X dirID - write operation skipped', 0x0d, 0
																		; 0x1fda
	
	DC.B 'CheckSwitchBoot: Stopping without actually attempting write operation', 0x0d, 0
																		; 0x2026
	
	DC.B 'Stopping at end of FCODE, as requested.', 0x0d, 0				; 0x206d
	DC.B 'reset-all', 0													; 0x2096
	DC.B 'AAPL,debug bit settings (-OR- bits together):', 0x0d, 0		; 0x20a0
	DC.B '%-8X', 0														; 0x20cf
	DC.B ' * ', 0														; 0x20d4
	DC.B '   ', 0														; 0x20d8
	DC.B '= Print general informative messages.', 0x0d, 0				; 0x20dc
	
	DC.B '= Print formatted Mac OS tables (except config/universal info).', 0x0d, 0
																		; 0x2103
	
	DC.B '= Print formatted config info table.', 0x0d, 0				; 0x2144
	DC.B '= Dump Mac OS tables (except config/universal info).', 0x0d, 0; 0x216a
	DC.B '= Print node names while copying the device tree.', 0x0d, 0	; 0x21a0
	DC.B '= Print property info while copying the device tree.', 0x0d, 0; 0x21d3
	DC.B '= Print interrupt-related info.', 0x0d, 0						; 0x2209
	DC.B '= Print interrupt tree traversal info.', 0x0d, 0				; 0x222a
	DC.B '= Print address resolution info.', 0x0d, 0					; 0x2252
	DC.B '= Print NV-RAM info.', 0x0d, 0								; 0x2274
	DC.B '= Print Mac OS "universal" info.', 0x0d, 0					; 0x228a
	DC.B '= Print "special" node info.', 0x0d, 0						; 0x22ac
	
	DC.B '= Load EtherPrintf utility via parcel for post FCode debugging.', 0x0d, 0
																		; 0x22ca
	
	DC.B '= Print BOOTP/DHCP/BSDP information.', 0x0d, 0				; 0x230b
	DC.B '= Allocate writable ROM aperture.', 0x0d, 0					; 0x2331
	DC.B '= Mark Toolbox image as non-cacheable.', 0x0d, 0				; 0x2354
	DC.B '= Print parcel info while copying the device tree.', 0x0d, 0	; 0x237c
	DC.B '= Print information on device tree data checksums.', 0x0d, 0	; 0x23b0
	DC.B '= Enable the Nanokernel debugger.', 0x0d, 0					; 0x23e4
	DC.B '= Display the Nanokernel log during boot.', 0x0d, 0			; 0x2407
	DC.B '= Dont attempt to unhibernate system.', 0x0d, 0				; 0x2432
	
	DC.B '= Halt after end of FCode (useful if outputting to screen).', 0x0d, 0
																		; 0x2459
	
	DC.B '/options', 0													; 0x2496
	DC.B 'aapl,debug', 0												; 0x249f
	
	DC.B '### AAPL,debug value in ''aapl,debug'' is invalid (''%s'')', 0x0d, 0
																		; 0x24aa
	
	DC.B '/chosen', 0													; 0x24e2
	DC.B 'bootpath', 0													; 0x24ea
	DC.B 'MacOS: unable to find bootpath', 0							; 0x24f3
	DC.B 'stdout', 0													; 0x2512
	DC.B 'memory', 0													; 0x2519
	
	DC.B 'MacOS: unable to find ihandle to Memory in "/chosen".', 0x0d, 0
																		; 0x2520
	
	DC.B 'AAPL,debug', 0												; 0x2557
	DC.B 'AAPL,writable-ROM-aperture', 0								; 0x2562
	DC.B 'copyright', 0													; 0x257d
	DC.B 'Copyright ', 0												; 0x2587
	DC.B ' Apple Computer, Inc.', 0										; 0x2592
	DC.B 'Official Apple copyright message missing.', 0x0d, 0			; 0x25a8
	DC.B 'canon', 0														; 0x25d3
	DC.B 'device_type', 0												; 0x25d9
	DC.B 'MacOS: -claim- for work area failed!', 0x0d, 0				; 0x25e5
	
	DC.B 'work area logical address = 0x%X, physical address = 0x%X.', 0x0d, 0
																		; 0x260b
	
	DC.B 'AAPL,cpu-id', 0												; 0x2647
	DC.B '/rom/macos', 0												; 0x2653
	DC.B 'AAPL,toolbox-parcels', 0										; 0x265e
	DC.B 'MacOS: Find of MacROM parcel failed!', 0x0d, 0				; 0x2673
	DC.B 'MacOS: parcel initialization failure - bad checksum', 0x0d, 0	; 0x2699
	
	DC.B 'MacOS: -claim- for toolbox image decompression area failed!', 0x0d, 0
																		; 0x26ce
	
	DC.B 'interrupt-controller', 0										; 0x270b
	
	DC.B 'Checking root interrupt-controller - is it Heathrow or Paddington?', 0x0d, 0
																		; 0x2720
	
	DC.B '/pci/mac-io/interrupt-controller', 0							; 0x2764
	DC.B 'Heathrow location method better get fixed.', 0x0d, 0			; 0x2785
	DC.B 'compatible', 0												; 0x27b1
	DC.B 'heathrow', 0													; 0x27bc
	DC.B 'paddington', 0												; 0x27c5
	DC.B 'Found a Heathrow interrupt controller!', 0x0d, 0				; 0x27d0
	DC.B 'PMR&BGsTree', 0												; 0x27f8
	
	DC.B 'MacOS: found RTAS replacement plug-ins -- tossing RTAS.', 0x0d, 0
																		; 0x2804
	
	DC.B 'MacOS: Neither RTAS nor plug-ins found and installed!', 0x0d, 0
																		; 0x283d
	
	DC.B 'MacOS: unable to find an interrupt controller node.', 0x0d, 0	; 0x2874
	DC.B 'MacOS: could not create universal product tables.', 0x0d, 0	; 0x28a9
	DC.B 'Device Tree is too large to fit into the work area!', 0x0d, 0	; 0x28dc
	DC.B 0x0d, 'FreeBytes address:       logical = 0x%08X', 0x0d, 0		; 0x2911
	DC.B 'WorkArea_target address: logical = 0x%08X', 0x0d, 0			; 0x293d
	
	DC.B 'SystemInfo addresses:    logical = 0x%08X, physical = 0x%08X', 0x0d, 0
																		; 0x2968
	
	DC.B 'ProcessorInfo addresses: logical = 0x%08X, physical = 0x%08X', 0x0d, 0
																		; 0x29a6
	
	DC.B 'HWInfo addresses:        logical = 0x%08X, physical = 0x%08X', 0x0d, 0
																		; 0x29e4
	
	DC.B 'HwInitInfo addresses:    logical = 0x%08X, physical = 0x%08X', 0x0d, 0
																		; 0x2a22
	
	DC.B 'NanoKernelEntry addresses: 0x%08X', 0x0d, 0x0d, 0				; 0x2a60
	DC.B 'System info:', 0x0d, 0										; 0x2a84
	DC.B 'Processor info:', 0x0d, 0										; 0x2a92
	DC.B 'Hardware info:', 0x0d, 0										; 0x2aa3
	DC.B 'HardwareInit info:', 0x0d, 0									; 0x2ab3
	DC.B 'Cascade Info:', 0x0d, 0										; 0x2ac7
	DC.B 'Can''t add workarea memory relocation entry', 0x0d, 0			; 0x2ad6
	DC.B 'Can''t add configinfo memory relocation entry', 0x0d, 0		; 0x2b02
	DC.B 'Can''t add vectorlookuptable memory relocation entry', 0x0d, 0; 0x2b30
	DC.B 'Can''t add vectormasktable memory relocation entry', 0x0d, 0	; 0x2b65
	DC.B 'Can''t add CascadeInfo memory relocation entry', 0x0d, 0		; 0x2b98
	
	DC.B 'Can''t add VectorPriorityTable memory relocation entry', 0x0d, 0
																		; 0x2bc7
	
	DC.B 'Can''t add rtas address range entry', 0x0d, 0					; 0x2bfe
	DC.B 'Can''t add processorinfo address range entry', 0x0d, 0		; 0x2c22
	DC.B 'Can''t add systeminfo address range entry', 0x0d, 0			; 0x2c4f
	DC.B 'Can''t add hwinfo address range entry', 0x0d, 0				; 0x2c79
	DC.B 'Can''t add hwinitinfo address range entry', 0x0d, 0			; 0x2c9f
	
	DC.B 'Stopping at end of FCODE, due to fatal error (see above).', 0x0d, 0
																		; 0x2cc9
	
	DC.B 'Off to MacOS.  The next (and last) call into OpenFirmware is quiesce().', 0x0d, 0
																		; 0x2d04
	
	DC.B 'quiesce', 0													; 0x2d4d
	DC.B 0																; 0x2d55
	DC.B 0																; 0x2d56
	DC.B 0																; 0x2d57
	DC.B 0																; 0x2d58
	DC.B 0																; 0x2d59
	DC.B 0																; 0x2d5a
	DC.B 0																; 0x2d5b
	DC.B 0																; 0x2d5c
	DC.B 0																; 0x2d5d
	DC.B 0																; 0x2d5e
	DC.B 0																; 0x2d5f
	DC.B 0																; 0x2d60
	DC.B 0																; 0x2d61
	DC.B 0																; 0x2d62
	DC.B 0																; 0x2d63
	DC.B 0																; 0x2d64
	DC.B 0																; 0x2d65
	DC.B 0																; 0x2d66
	DC.B 0																; 0x2d67
	DC.B 'Found parcel ''%s'', type ''%s''.', 0x0d, 0					; 0x2d68
	
	DC.B 'device_type ''%s'': Matched parcel ''%s'', device_type ''%s''.', 0x0d, 0
																		; 0x2d87
	
	DC.B 'node name ''%s'': Matched parcel ''%s'', device_type ''%s''.', 0x0d, 0
																		; 0x2dc1
	
	DC.B 'parent name ''%s'': Matched parcel ''%s'', device_type ''%s''.', 0x0d, 0
																		; 0x2df9
	
	DC.B 'compatible ''%s'': Matched parcel ''%s'', device_type ''%s''.', 0x0d, 0
																		; 0x2e33
	
