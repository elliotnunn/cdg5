elfstart:
	DC.L	0x7f454c46					; 0x00 magic number
	DC.B	1							; 0x04 32-bit
	DC.B	2							; 0x05 big-endian
	DC.B	1							; 0x06 ELF v1
	DC.B	0							; 0x07 SysV or unspecified ABI
	DC.B	0							; 0x08 ABI ver
	DC.B	0							; 0x09 unused
	DC.B	0
	DC.B	0
	DC.B	0
	DC.B	0
	DC.B	0
	DC.B	0
	
	DC.W	2							; 0x10 executable
	DC.W	0x14						; 0x12 PowerPC
	DC.L	1							; 0x14 original ELF
	DC.L	0x20f078					; 0x18 entry point address
	DC.L	progheader					; 0x1c program header offset
	DC.L	0							; 0x20 section header offset
	DC.L	0							; 0x24 flags
	DC.W	progheader					; 0x28 size of this header
	DC.W	0x20						; 0x2a size of a prog header entry
	DC.W	3							; 0x2c program header entry count
	DC.W	0							; 0x2e size of a section header entry
	DC.W	0							; 0x30 section header entry count
	DC.W	0							; 0x32 section names in which section header entry?
	
progheader:
	DC.L	4							; PT_NOTE segment
	DC.L	noteSegStart
	DC.L	0							; virtual address of segment
	DC.L	0							; physical address of segment
	DC.L	noteSegEnd-noteSegStart
	DC.L	0							; size in memory
	DC.L	0							; flags
	DC.L	0							; no alignment
	
	DC.L	1							; PT_LOAD segment
	DC.L	codeSegStart
	DC.L	0x00200000					; virtual address of segment
	DC.L	0x00200000					; physical address of segment
	DC.L	codeSegEnd-codeSegStart
	DC.L	codeSegEnd-codeSegStart
	DC.L	0x00000005					; flags
	DC.L	0x1000						; page-aligned

	DC.L	1							; PT_LOAD segment
	DC.L	dataSegStart
	DC.L	0x00100000					; virtual address of segment
	DC.L	0x00100000					; physical address of segment
	DC.L	dataSegEnd-dataSegStart
	DC.L	0x19968						; size in memory, � 0x6c08 in file
	DC.L	0x00000006					; flags
	DC.L	0x1000						; page-aligned

noteSegStart:
	INCLUDE 'NoteSegment.s'
noteSegEnd:
	
	ALIGN 8

codeSegStart:
	INCLUDE 'Code.s'
codeSegEnd:
	
	ALIGN 8

dataSegStart:
	INCLUDE 'DataBinary1.s'
	INCLUDE 'DataString1.s'
	INCLUDE 'DataBinary2.s'
	INCLUDE 'DataString2.s'
	INCLUDE 'DataBinary3.s'
	INCLUDE 'DataString3.s'
	INCLUDE	'DataBinary4.s'
	INCLUDE 'DataString4.s'
	INCLUDE 'DataBinary5.s'
	INCLUDE 'DataString5.s'
dataSegEnd:
