	STRING   AsIs
	
	DC.B 0x0d, 'Creating dynamic ProductInfo & Friends', 0x0d, 0		; 0x2f18
	DC.B '-- Dynamic ProductInfoPtr (logical) = %08X', 0x0d, 0			; 0x2f41
	DC.B '-- SizeOf( ProductInfo )  = %d', 0x0d, 0						; 0x2f6d
	
	DC.B '-- Converting UniveralInfoTableBase to 68k logical address %08X.', 0x0d, 0
																		; 0x2f8d
	
	DC.B 'Initializing ProductInfo record', 0x0d, 0						; 0x2fcf
	DC.B '-- Initializing productInfoVers to %d.', 0x0d, 0				; 0x2ff0
	DC.B 'Initializing static section of DecoderInfoPrivate.', 0x0d, 0	; 0x3018
	DC.B 'Initializing DecoderTable.', 0x0d, 0							; 0x304c
	
	DC.B '-- Initializing (logical) ROMAddr (which is ALWAYS %08X).', 0x0d, 0
																		; 0x3068
	
	DC.B '-- Clock primitives accessed via RTAS.', 0x0d, 0				; 0x30a3
	DC.B '-- Initializing OpenPICBaseAddr.', 0x0d, 0					; 0x30cb
	DC.B '---- OpenPICAddr = %08X.', 0x0d, 0							; 0x30ed
	DC.B '-- Initializing HeathrowBaseAddr.', 0x0d, 0					; 0x3107
	DC.B '---- HeathrowAddr = %08X.', 0x0d, 0							; 0x312a
	DC.B '-- Initializing VIA1 address.', 0x0d, 0						; 0x3145
	DC.B 'ERROR - could not find address of VIA1', 0x0d, 0				; 0x3164
	DC.B '---- VIA1Addr = %08X.', 0x0d, 0								; 0x318c
	DC.B '-- Initializing SCC addresses.', 0x0d, 0						; 0x31a3
	DC.B '---- SCC base address = %08X.', 0x0d, 0						; 0x31c3
	DC.B '---- No SCC (of some sort) detected!', 0x0d, 0				; 0x31e2
	DC.B '-- Initializing Mesh SCSI information.', 0x0d, 0				; 0x3208
	DC.B '---- Mesh SCSI base address = %08X.', 0x0d, 0					; 0x3230
	DC.B '-- Initializing ADB information.', 0x0d, 0					; 0x3255
	DC.B '---- ADB (of some sort) detected.', 0x0d, 0					; 0x3277
	DC.B '---- ADB base address = %08X.', 0x0d, 0						; 0x329a
	DC.B '---- Virtual (USB-emulated) ADB detected!', 0x0d, 0			; 0x32b9
	DC.B '---- No ADB (of some sort) detected!', 0x0d, 0				; 0x32e4
	DC.B '-- ASSUMPTION -- we ALWAYS have some sort of ATA.', 0x0d, 0	; 0x330a
	DC.B '-- Floppy controller base address = %08X', 0x0d, 0			; 0x333d
	DC.B '-- Initializing Sound information.', 0x0d, 0					; 0x3367
	DC.B '-- Has Awacs Sound Chip.', 0x0d, 0							; 0x338b
	DC.B '---- No Sound (of some sort) detected!', 0x0d, 0				; 0x33a5
	DC.B '-- Initializing extValid bits.', 0x0d, 0						; 0x33cd
	DC.B '---- extValid bits set to %08X.', 0x0d, 0						; 0x33ed
	DC.B '-- Initializing extValid1 bits.', 0x0d, 0						; 0x340e
	DC.B '---- extValid1 bits set to %08X.', 0x0d, 0					; 0x342f
	DC.B '-- Initializing HwCfgWord bits.', 0x0d, 0						; 0x3451
	DC.B '---- HwCfgWord bits set to %04X.', 0x0d, 0					; 0x3472
	DC.B 0x0d, 'Dynamic ProductInfo Table:', 0x0d, 0					; 0x3494
	DC.B 'Dynamic DecoderPrivateInfo Table:', 0x0d, 0					; 0x34b1
	DC.B 'Dynamic DecoderInfo Table:', 0x0d, 0							; 0x34d4
	DC.B 'parent', 0													; 0x34f0
	DC.B 'getprop', 0													; 0x34f7
	DC.B 'device_type', 0												; 0x34ff
	DC.B 'pci', 0														; 0x350b
	DC.B '#address-cells', 0											; 0x350f
	DC.B '#size-cells', 0												; 0x351e
	DC.B 'getproplen', 0												; 0x352a
	DC.B 'ranges', 0													; 0x3535
	DC.B 'CardBus ''ranges'' property:', 0x0d, 0						; 0x353c
	
	DC.B '** addressCells == %d, parents addressCells == %d, sizeCells == %d', 0x0d, 0
																		; 0x3558
	
	DC.B 'CreateCardBusProperties: Memory Space Base Addr = 0x%08X.', 0x0d, 0
																		; 0x359c
	
	DC.B 'CreateCardBusProperties: Memory Space Size      = 0x%08X.', 0x0d, 0
																		; 0x35d7
	
	DC.B 'AAPL,reserved-memory-space', 0								; 0x3612
	DC.B 'setprop', 0													; 0x362d
	
	DC.B 'CreateCardBusProperties: more than one memory space in ''ranges'' property - ignored.', 0x0d, 0
																		; 0x3635
	
	DC.B 'CreateCardBusProperties: Memory IO Base Addr     = 0x%08X.', 0x0d, 0
																		; 0x368a
	
	DC.B 'CreateCardBusProperties: Memory IO Size          = 0x%08X.', 0x0d, 0
																		; 0x36c6
	
	DC.B 'AAPL,reserved-io-space', 0									; 0x3702
	
	DC.B 'CreateCardBusProperties: more than one IO space in ''ranges'' property - ignored.', 0x0d, 0
																		; 0x3719
	
	DC.B 'matching assigned address:', 0x0d, 0							; 0x376a
	DC.B 'assigned-addresses', 0										; 0x3786
	DC.B 'reg', 0														; 0x3799
	
	DC.B 'HandleSpecialNode: SCSI device base address = 0x%08X', 0x0d, 'HandleSpecialNode: SCSI DMA base address    = 0x%08X.', 0x0d, 0
																		; 0x379d
	
	DC.B 'AAPL,not-fast', 0												; 0x3809
	DC.B 'HandleSpecialNode: AAPL,not-fast detected.', 0x0d, 0			; 0x3817
	DC.B 'AAPL,not-synchronous', 0										; 0x3843
	DC.B 'HandleSpecialNode: AAPL,not-synchronous detected.', 0x0d, 0	; 0x3858
	DC.B 'HandleSpecialNode: SCC legacy base address = 0x%08X.', 0x0d, 0; 0x388b
	DC.B 'slot-names', 0												; 0x38c1
	DC.B 'HandleSpecialNode: ch-a slot-name detected.', 0x0d, 0			; 0x38cc
	DC.B 'HandleSpecialNode: ch-b slot-name detected.', 0x0d, 0			; 0x38f9
	DC.B 'HandleSpecialNode: VIA base address = 0x%08X.', 0x0d, 0		; 0x3926
	
	DC.B 'HandleSpecialNode: Cuda detected, VIA base address = 0x%08X.', 0x0d, 0
																		; 0x3955
	
	DC.B 'HandleSpecialNode: Cuda Power Mgt detected.', 0x0d, 0			; 0x3993
	DC.B 'mgt-kind', 0													; 0x39c0
	DC.B 'min-consumption-pwm-led', 0									; 0x39c9
	DC.B 'HandleSpecialNode: PMU Power Mgt detected.', 0x0d, 0			; 0x39e1
	DC.B 'HandleSpecialNode: Core 99 Power Mgt detected.', 0x0d, 0		; 0x3a0d
	DC.B 'no-pmu', 0													; 0x3a3d
	DC.B 'HandleSpecialNode: LCD Backlight detected.', 0x0d, 0			; 0x3a44
	DC.B 'HandleSpecialNode: Cardbus detected.', 0x0d, 0				; 0x3a70
	DC.B 'HandleSpecialNode: Cuda ADB detected.', 0x0d, 0				; 0x3a96
	DC.B 'HandleSpecialNode: PMU ADB detected.', 0x0d, 0				; 0x3abd
	DC.B 'HandleSpecialNode: P99 ADB detected.', 0x0d, 0				; 0x3ae3
	DC.B 'HandleSpecialNode: ADB base address = 0x%08X.', 0x0d, 0		; 0x3b09
	DC.B 'AAPL,has-embedded-fn-keys', 0									; 0x3b38
	DC.B 'HandleSpecialNode: Embedded function keys detected.', 0x0d, 0	; 0x3b52
	DC.B 'compatible', 0												; 0x3b87
	DC.B 'trackpad', 0													; 0x3b92
	DC.B 'HandleSpecialNode: Trackpad detected.', 0x0d, 0				; 0x3b9b
	DC.B 'pmu', 0														; 0x3bc2
	DC.B 'interrupt-parent', 0											; 0x3bc6
	
	DC.B 'HandleSpecialNode: Host OpenPIC has base address = 0x%08X.', 0x0d, 0
																		; 0x3bd7
	
	DC.B 'MacOS: found a cascading OpenPIC without a matching parent bridge', 0x0d, 0
																		; 0x3c13
	
	DC.B 'HandleSpecialNode: a cascading OpenPIC has base address = 0x%08X.', 0x0d, 0
																		; 0x3c56
	
	DC.B 'HandleSpecialNode: Heathrow base address = 0x%08X.', 0x0d, 0	; 0x3c99
	DC.B 'HandleSpecialNode: Mac-IO base address = 0x%08X.', 0x0d, 0	; 0x3ccd
	DC.B 'HandleSpecialNode: Starlifter #%d found.', 0x0d, 0			; 0x3cff
	DC.B 'MacOS: Too many Starlifters!', 0x0d, 0						; 0x3d29
	DC.B 'HandleSpecialNode: SWIM3 base address = 0x%08X.', 0x0d, 0		; 0x3d47
	DC.B 'HandleSpecialNode: Awacs base address = 0x%08X.', 0x0d, 0		; 0x3d78
	
	DC.B 'HandleSpecialNode: KeyLargo timer base address = 0x%08X.', 0x0d, 0
																		; 0x3da9
	
	DC.B 'HandleSpecialNode: a node was deemed special but we do not know how to treat it specially', 0x0d, 0
																		; 0x3de3
	
	DC.B '                   ID = %d', 0x0d, 0							; 0x3e3e
	DC.B 0																; 0x3e5a
	DC.B 0																; 0x3e5b
	DC.B 0																; 0x3e5c
	DC.B 0																; 0x3e5d
	DC.B 0																; 0x3e5e
	DC.B 0																; 0x3e5f
