	STRING   AsIs
	
	DC.B '-2147483648', 0												; 0x6b90
	DC.B 'Dumping %d bytes @ 0x%08X', 0x0d, 0							; 0x6b9c
	DC.B '%08X: %08X %08X %08X %08X', 0x0d, 0							; 0x6bb7
	DC.B '%08X: %08X %08X %08X', 0x0d, 0								; 0x6bd2
	DC.B '%08X: %08X %08X', 0x0d, 0										; 0x6be8
	DC.B '%08X: %08X', 0x0d, 0											; 0x6bf9
	DC.B 0x0d, 0														; 0x6c05
	DC.B 0																; 0x6c07
