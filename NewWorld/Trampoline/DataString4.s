	STRING   AsIs
	
	DC.B 'OpenPIC setup: vectorIndex %d, intSource %d, level %d, sense %d, polarity %d', 0x0d, 0
																		; 0x4800
	
	DC.B 'OpenPIC @ %08X setup: index %d - illegal intSource value (%X) - ignoring', 0x0d, 0
																		; 0x484e
	
	DC.B 'OpenPIC @ %08X setup: setting interrupt source %2d to 0x%08X', 0x0d, 0
																		; 0x4898
	
	DC.B 'getprop', 0													; 0x48d6
	DC.B 'interrupt-parent', 0											; 0x48de
	DC.B '#interrupt-cells', 0											; 0x48ef
	DC.B 'parent', 0													; 0x4900
	DC.B 'RTCServicesLib', 0											; 0x4907
	DC.B 'MacOS: Found a plug-in for RTC.', 0x0d, 0						; 0x4916
	DC.B 'NVRAMServicesLib', 0											; 0x4937
	DC.B 'MacOS: Found a plug-in for NVRAM.', 0x0d, 0					; 0x4948
	DC.B 'code,AAPL,MacOS,name', 0										; 0x496b
	DC.B 'PowerMgrPlugin', 0											; 0x4980
	DC.B 'MacOS: Found a PowerPlugin.', 0x0d, 0							; 0x498f
	DC.B 'device_type', 0												; 0x49ac
	DC.B 'pci', 0														; 0x49b8
	DC.B 'reg', 0														; 0x49bc
	DC.B 'slot-names', 0												; 0x49c0
	DC.B '%s:', 0														; 0x49cb
	DC.B ' %08X', 0														; 0x49cf
	DC.B 0x0d, 0														; 0x49d5
	DC.B 'name', 0														; 0x49d7
	
	DC.B '---------------- Node ''%s'' has %d interrupt(s). ----------------', 0x0d, 0
																		; 0x49dc
	
	DC.B 'MacOS: device has too many interrupts!', 0x0d, 0				; 0x4a1e
	DC.B 'MacOS: device has < 1 interrupts!', 0x0d, 0					; 0x4a46
	DC.B 'AAPL,requested-priorities', 0									; 0x4a69
	DC.B 'Requested priorities for this node:', 0x0d, 0					; 0x4a83
	DC.B 'getproplen', 0												; 0x4aa8
	DC.B '#address-cells', 0											; 0x4ab3
	DC.B 'Processing unit_interrupt_specifier:', 0x0d, 0				; 0x4ac2
	DC.B 'Raw unit_int_specifier   ', 0									; 0x4ae8
	DC.B 'interrupt-controller', 0										; 0x4b02
	DC.B 'found a cascading interrupt controller @ 0x%08X', 0x0d, 0		; 0x4b17
	DC.B 'MacOS: too many cascading interrupt controllers', 0x0d, 0		; 0x4b48
	DC.B 'cascadeStack[%d] = %d (ua_size = %d)', 0x0d, 0				; 0x4b79
	DC.B '(ua_size = %d, is_size = %d) setting edge[%d] to %d', 0x0d, 0	; 0x4b9f
	DC.B 'ua_size = %d', 0x0d, 0										; 0x4bd4
	DC.B 'New UIS from ''reg'' prop  ', 0								; 0x4be2
	DC.B 'interrupts', 0												; 0x4bfc
	DC.B 'interrupt tree error at node 0x%08X', 0x0d, 0					; 0x4c07
	
	DC.B 'MacOS: missing ''interrupts'' property in cascading interrupt controller node!', 0x0d, 0
																		; 0x4c2c
	
	DC.B 'Concatenate UA with IS (ua_size=%d, is_size=%d):', 0x0d, 0	; 0x4c7a
	DC.B 'New UIS w/''interrupts'' added ', 0							; 0x4cac
	DC.B 'interrupt-map', 0												; 0x4cca
	DC.B 'interrupt-map-mask', 0										; 0x4cd8
	DC.B 'Interrupt-map-mask values:', 0								; 0x4ceb
	DC.B 'MacOS: "interrupt-map" larger than expected.', 0x0d, 0		; 0x4d06
	
	DC.B 0x0d, 'Comparing interrupt-map entries to this unit_interrupt_specifier:', 0x0d, 0x0d, 0
																		; 0x4d34
	
	DC.B 'Masked unit_int_specifier', 0									; 0x4d79
	DC.B 'MacOS: missing ''#interrupt-cells''.', 0x0d, 0				; 0x4d93
	DC.B 'Interrupt-map child spec ', 0									; 0x4db7
	
	DC.B '***** MacOS: the following error causes your machine not to be able to boot: *****', 0x0d, 0
																		; 0x4dd1
	
	DC.B '***** MacOS: unit-interrupt-specifier for ''%s'' not found in map. *****', 0x0d, 0
																		; 0x4e25
	
	DC.B 'New unit_int_specifier   ', 0									; 0x4e6d
	DC.B 'unit_interrupt_specifer[%d] = %d', 0x0d, 0					; 0x4e87
	DC.B 'cascade value %d = %d', 0x0d, 0								; 0x4ea9
	
	DC.B 'MacOS: cascaded interrupt vector not found in host vector list!', 0x0d, 0
																		; 0x4ec0
	
	DC.B 'MacOS: cascaded interrupt, but no cascading bridge!', 0x0d, 0	; 0x4f01
	DC.B 'MacOS: too many cascaded interrupt sources!', 0x0d, 0			; 0x4f36
	DC.B 'slotBitMap[%d] = %04X', 0x0d, 0								; 0x4f63
	DC.B 'MacOS: too many interrupt sources!', 0x0d, 0					; 0x4f7a
	DC.B 'MacOS: unable to handle shared cascaded interrupts!', 0x0d, 0	; 0x4f9e
	DC.B 'AAPL,interrupt-vectors', 0									; 0x4fd3
	DC.B 'compatible', 0												; 0x4fea
	DC.B 'AAPL,interrupt-priorities', 0									; 0x4ff5
	DC.B 'AAPL,interrupts', 0											; 0x500f
	DC.B 'AAPL,interrupt-index', 0										; 0x501f
	DC.B 'node ''%s'': decompressing property ''%s''.', 0x0d, 0			; 0x5034
	
	DC.B 'MacOS: bad parcelDataCompressorType ''%s''', 0x0d, ', node name ''%s'', parcelPropertyName ''%s''', 0x0d, 0
																		; 0x505d
	
	DC.B 'Creating property - ''%s'' (size = %d)', 0x0d, 0				; 0x50b1
	
	DC.B 'Creating property ''%s'' (size = 0x%X); stored at 0x%X. Belongs to devNode 0x%X', 0x0d, 0
																		; 0x50d7
	
	DC.B ' -- should never get to this routine and have ''mac-io'' base address == zero!', 0x0d, 0
																		; 0x5126
	
	DC.B 'AAPL,address', 0												; 0x5174
	DC.B 'MacOS: Too many assigned address entries!', 0x0d, 0			; 0x5181
	DC.B '-- ''AAPL,address'' already exists for this node!', 0x0d, 0	; 0x51ac
	DC.B 'finddevice', 0												; 0x51dd
	DC.B '/aliases', 0													; 0x51e8
	DC.B 'screen', 0													; 0x51f1
	DC.B 'peer', 0														; 0x51f8
	DC.B 0																; 0x51fd
	DC.B 'created node ''%s''', 0x0d, 0									; 0x51fe
	DC.B 'parcel ''%s'': Added property ''%s''.', 0x0d, 0				; 0x5211
	DC.B 'model', 0														; 0x5234
	DC.B 'display', 0													; 0x523a
	DC.B 'package-to-path', 0											; 0x5242
	DC.B ':0', 0														; 0x5252
	DC.B 'open', 0														; 0x5255
	DC.B 'depth', 0														; 0x525a
	DC.B 'display-type', 0												; 0x5260
	DC.B 'CRT', 0														; 0x526d
	DC.B 'setprop', 0													; 0x5271
	DC.B 'AAPL,gray-page', 0											; 0x5279
	DC.B 'LCD', 0														; 0x5288
	DC.B 'dimensions', 0												; 0x528c
	
	DC.B 'node ''%s'': Parcel ''%s'' not loaded (not required for boot).', 0x0d, 0
																		; 0x5297
	
	DC.B 'nextprop', 0													; 0x52d3
	DC.B '  Copying peer property ''%s'' stored at 0x%X', 0x0d, 0		; 0x52dc
	
	DC.B '  Copying first property ''%s'' stored at 0x%X, devNode 0x%X', 0x0d, 0
																		; 0x5309
	
	DC.B 'node ''%s'': Replaced property ''%s''.', 0x0d, 0				; 0x5345
	DC.B 'node ''%s'': Deleted property ''%s''.', 0x0d, 0				; 0x5369
	DC.B 'node ''%s'': Did NOT replace property ''%s''.', 0x0d, 0		; 0x538c
	DC.B 'assigned-addresses', 0										; 0x53b7
	
	DC.B 'node ''%s'': Property NOT added (already loaded) ''%s''.', 0x0d, 0
																		; 0x53ca
	
	DC.B 'node ''%s'': Did NOT add property ''%s''.', 0x0d, 0			; 0x5400
	DC.B 'parcel node not found, parcel ''%s'' NOT added.', 0x0d, 0		; 0x5427
	DC.B 'Added property ''%s'' to special node %d.', 0x0d, 0			; 0x5456
	DC.B 'node ''%s'': Added property ''%s''.', 0x0d, 0					; 0x547f
	DC.B 'CRC changed from 0x%X to 0x%X, node ''%s''', 0x0d, 0			; 0x54a0
	DC.B 'CRC disabled for node ''%s''.', 0x0d, 0						; 0x54ca
	DC.B 'CRC unchanged at 0x%X, node ''%s''', 0x0d, 0					; 0x54e7
	DC.B 'child', 0														; 0x5509
	DC.B 'Copying child device node ''%s'' of node at 0x%X.', 0x0d, 0	; 0x550f
	DC.B 'Copying device node peer ''%s'' of node at 0x%X.', 0x0d, 0	; 0x5540
	DC.B 'Final CRC ''0x%X''', 0x0d, 0									; 0x5570
	DC.B 0																; 0x5582
	DC.B 0																; 0x5583
	DC.B 0																; 0x5584
	DC.B 0																; 0x5585
	DC.B 0																; 0x5586
	DC.B 0																; 0x5587
	
	DC.B 'MacOS: InitializePageMapTable unable to Claim pageMapEntry array', 0x0d, 0
																		; 0x5588
	
	DC.B 'AddPageMapEntry: LA = 0x%08X, count = 0x%04X, PA = 0x%08X, pageAttr = 0x%04X, flags = 0x%02X.', 0x0d, 0
																		; 0x55ca
	
	DC.B 'Next free PageMap entry for segment 0x%X is entry %d.', 0x0d, 0
																		; 0x5629
	
	DC.B 'AddPageMapTableEntry: extending pageMapEntry array for segment 0x%X', 0x0d, 0
																		; 0x5660
	
	DC.B 'MacOS: AddPageMapTableEntry unable to Claim larger PageMap array.', 0x0d, 0
																		; 0x56a5
	
	DC.B 'MacOS: AddPageMapTableEntry: overlapping size larger than existing size! (1)', 0x0d, 0
																		; 0x56e8
	
	DC.B 'MacOS: AddPageMapTableEntry: overlapping size larger than overlapped entry! (2)', 0x0d, 0
																		; 0x5736
	
	DC.B '--- Dumping PageMapStateTable ---', 0x0d, 0					; 0x5787
	
	DC.B 'Segment = 0x%X, LA = 0x%08X, count = 0x%04X, PA = 0x%08X, pageAttr = 0x%04X, flags = 0x%02X.', 0x0d, 0
																		; 0x57aa
	
	DC.B 'Segment 0x%X has too many page table entries -- mapping the entire segment.', 0x0d, 0
																		; 0x5808
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in AllocatePagesForDevices.', 0x0d, 0
																		; 0x5855
	
	DC.B 'finddevice', 0												; 0x58ad
	DC.B '/aliases', 0													; 0x58b8
	DC.B 'getproplen', 0												; 0x58c1
	DC.B 'screen', 0													; 0x58cc
	DC.B 'getprop', 0													; 0x58d3
	DC.B 'address', 0													; 0x58db
	DC.B 'width', 0														; 0x58e3
	DC.B 'height', 0													; 0x58e9
	DC.B 'linebytes', 0													; 0x58f0
	DC.B 'depth', 0														; 0x58fa
	
	DC.B 'ToolboxImage current logical address = 0x%08X, ToolboxImage final physical address = 0x%08X.', 0x0d, 0
																		; 0x5900
	
	DC.B 'ConfigInfo logical address in current ROM image = 0x%08X.', 0x0d, 0
																		; 0x595e
	
	DC.B 'configinfop->ROMImageBaseOffset address  = 0x%08X.', 0x0d, 0	; 0x5999
	DC.B 'configinfop->Mac68KROMOffset address     = 0x%08X.', 0x0d, 0	; 0x59cd
	DC.B 'configinfop->HWInitCodeOffset address    = 0x%08X.', 0x0d, 0	; 0x5a01
	DC.B 'configinfop->DiagPEFBundleOffset address = 0x%08X.', 0x0d, 0	; 0x5a35
	DC.B 'configinfop->KernelCodeOffset address    = 0x%08X.', 0x0d, 0	; 0x5a69
	DC.B 'configinfop->EmulatorCodeOffset address  = 0x%08X.', 0x0d, 0	; 0x5a9d
	DC.B 'configinfop->OpcodeTableOffset address   = 0x%08X.', 0x0d, 0	; 0x5ad1
	
	DC.B 'MacOS: boot failure - Cannot allocate memory for page map table creation.', 0x0d, 0
																		; 0x5b05
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for segment F.', 0x0d, 0
																		; 0x5b50
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for info records.', 0x0d, 0
																		; 0x5bb6
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for debugging aperture.', 0x0d, 0
																		; 0x5c1f
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for emulator data page.', 0x0d, 0
																		; 0x5c8e
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for kernel data page.', 0x0d, 0
																		; 0x5cfd
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for OpenPIC interrupt controller.', 0x0d, 0
																		; 0x5d6a
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for Heathrow interrupt controller.', 0x0d, 0
																		; 0x5de3
	
	DC.B 'MacOS: boot failure - Cannot create new page table mapping in page map table creation for vector data page.', 0x0d, 0
																		; 0x5e5d
	
	DC.B 'ConfigInfo table:', 0x0d, 0									; 0x5eca
	DC.B 0																; 0x5edd
	DC.B 0																; 0x5ede
	DC.B 0																; 0x5edf
	
	DC.B '-- CopySomeStuff:  From:  %08X  To:  %08X  Length:  %d  MaxLength:  %d', 0x0d, 0
																		; 0x5ee0
	
	DC.B '-- CopySomeStuff:  Data size (%d) exceeds maximum safe length (%d)', 0x0d, 0
																		; 0x5f28
	
	DC.B '%d', 0														; 0x5f6c
	DC.B '.', 0															; 0x5f6f
	DC.B 'TakeApartFilenameTags:  IP   = %08X (', 0						; 0x5f71
	DC.B ')', 0x0d, 0													; 0x5f97
	DC.B 'TakeApartFilenameTags:  Port = %04X', 0x0d, 0					; 0x5f9a
	DC.B 'TakeApartFilenameTags:  Volume = ''%s'' (len = %d)', 0x0d, 0	; 0x5fbf
	DC.B 'TakeApartFilenameTags:  DirID = %08X', 0x0d, 0				; 0x5ff1
	DC.B 'TakeApartFilenameTags:  Path = ''', 0							; 0x6017
	DC.B '%s', 0														; 0x6038
	DC.B ':', 0															; 0x603b
	DC.B ''' (len = %d)', 0x0d, 0										; 0x603d
	DC.B 'Processing BOOTP/DHCP tags:', 0x0d, 0							; 0x604b
	DC.B '  %08X:  ', 0													; 0x6068
	DC.B 'Tag:  %d.', 0x0d, 0											; 0x6072
	DC.B 'Tag:  %d.  Len = %d', 0x0d, 0									; 0x607d
	DC.B 'Processing DHCP_TAG_END', 0x0d, 0								; 0x6092
	DC.B '  BSDP packet detected.  Processing ...', 0x0d, 0				; 0x60ab
	DC.B '  Processing DHCP_TAG_VENDOR_CLASS_ID:  ''%*s''', 0x0d, 0		; 0x60d4
	DC.B '  Processing DHCP_TAG_PAD', 0x0d, 0							; 0x6103
	DC.B '  Processing DHCP_TAG_SUBNET_MASK', 0x0d, 0					; 0x611e
	DC.B '  -- Subnet mask = %08X (', 0									; 0x6141
	DC.B '  Processing DHCP_TAG_DOMAIN_NAMESERVER', 0x0d, 0				; 0x615b
	DC.B '  [%d]: ', 0													; 0x6184
	DC.B '  IP = %08X (', 0												; 0x618d
	DC.B '  Processing DHCP_TAG_ROUTER_ADDR', 0x0d, 0					; 0x619b
	DC.B '  Processing DHCP_TAG_CLIENT_HOSTNAME', 0x0d, 0				; 0x61be
	DC.B '  Processing DHCP_TAG_MACOS_MACHINE_NAME', 0x0d, 0			; 0x61e5
	DC.B '  -- Machine Name is:  ''%s''', 0x0d, 0						; 0x620f
	DC.B '  Processing DHCP_TAG_EXTENSIONS_PATH', 0x0d, 0				; 0x622c
	DC.B '  Processing DHCP_TAG_SERVER_VERS', 0x0d, 0					; 0x6253
	DC.B '  Processing DHCP_TAG_SERVER_INFO', 0x0d, 0					; 0x6276
	DC.B '  Processing DHCP_TAG_USER_NAME', 0x0d, 0						; 0x6299
	DC.B '  -- Served-supplied user name:  ''%s''', 0x0d, 0				; 0x62ba
	DC.B '  Processing DHCP_TAG_PASSWORD', 0x0d, 0						; 0x62e1
	DC.B '  -- Served-supplied user password:  ''%s''', 0x0d, 0			; 0x6301
	DC.B '  Processing DHCP_TAG_SSW_BOOT', 0x0d, 0						; 0x632c
	DC.B '  Processing DHCP_TAG_SSW_PRIVATE', 0x0d, 0					; 0x634c
	DC.B '  Processing DHCP_TAG_VM_FILE', 0x0d, 0						; 0x636f
	DC.B '  Processing DHCP_TAG_SSW_SHADOW_BOOT', 0x0d, 0				; 0x638e
	DC.B '  Processing DHCP_TAG_SSW_SHADOW_PRIVATE', 0x0d, 0			; 0x63b5
	
	DC.B 'Processing a tag (%d, length: %d) we do not care about', 0x0d, 0
																		; 0x63df
	
	DC.B '-- Invalid len byte (%d).  Stopping tag processing.', 0x0d, 0	; 0x6417
	DC.B '''Random'' AFP port number = %d', 0x0d, 0						; 0x644c
	DC.B 'PRIVATE (extra) volume info:', 0x0d, 0						; 0x646b
	DC.B 'BOOT volume info:', 0x0d, 0									; 0x6489
	DC.B 'PAGING volume info:', 0x0d, 0									; 0x649c
	DC.B 'ERROR - unknown NetBoot image file type!', 0x0d, 0			; 0x64b1
	DC.B 'Cleartxt Passwrd', 0											; 0x64db
	DC.B 'SHADOW volume info:', 0x0d, 0									; 0x64ed
	DC.B 'Username: ''%s'' (len = %d)', 0x0d, 0							; 0x6502
	DC.B 'Password: ''%s''', 0x0d, 0									; 0x651d
	DC.B 'UAM:      ''%s'' (len = %d)', 0x0d, 0							; 0x652d
	DC.B 'finddevice', 0												; 0x6548
	DC.B '/chosen', 0													; 0x6553
	DC.B 'bootp-response', 0											; 0x655b
	DC.B 'getproplen', 0												; 0x656a
	
	DC.B '-- Storage for ''%s'' packet obtained.  Located at %08X, size = %X', 0x0d, 0
																		; 0x6575
	
	DC.B 'getprop', 0													; 0x65b7
	DC.B 'dhcp-response', 0												; 0x65bf
	
	DC.B '-- ERROR: could not allocate storage for local copy of ''%s'' packet.  No properties will be created!', 0x0d, 0
																		; 0x65cd
	
	DC.B 'bsdp-response', 0												; 0x6632
	
	DC.B '-- ''dhcp-response'' packet exists but ''bsdp-response'' does not.', 0
																		; 0x6640
	
	DC.B 'MacOS:  Netboot communications failure.  Unable to netboot.', 0x0d, 0
																		; 0x667f
	
	DC.B '-- Could not make local copy of ''%s'' packet.  No properties will be created!', 0x0d, 0
																		; 0x66bc
	
	DC.B '-- Storage for ''%s'' property obtained.  Located at %08X, size = %X', 0x0d, 0
																		; 0x670a
	
	DC.B 'AAPL,LANDiskInfo', 0											; 0x674e
	
	DC.B '-- Storage for miscellaneous information obtained.  Located at %08X, size = %X', 0x0d, 0
																		; 0x675f
	
	DC.B '-- ERROR: storage for ''%s'' overlaps ''%s'' packet!', 0x0d, 0; 0x67af
	
	DC.B '-- ERROR: storage for ''%s'' packet overlaps ''%s'' buffer!', 0x0d, 0
																		; 0x67e1
	
	DC.B '****************************************************************************', 0x0d, 0
																		; 0x681a
	
	DC.B '* WARNING:  EtherPrintf will be loaded in addition to NetBoot.  This will  *', 0x0d, 0
																		; 0x6868
	
	DC.B '*           cause Open Transport to be unable to load its ethernet driver. *', 0x0d, 0
																		; 0x68b6
	
	DC.B '-- ERROR: ''s'' packet was NOT a DHCP-style reply packet', 0x0d, 0
																		; 0x6904
	
	DC.B 'No network files located.  LANDiskInfo property not created.', 0x0d, 0
																		; 0x693c
	
	DC.B 0x0d, 'Property Table of Contents:', 0x0d, 0					; 0x697a
	DC.B '    Entry:  %08X', 0x0d, 0									; 0x6998
	DC.B 'Creating ''%s'' property in the ''/chosen'' node', 0x0d, 0	; 0x69aa
	DC.B 'setprop', 0													; 0x69d8
	DC.B 'AAPL,MacOSMachineName', 0										; 0x69e0
	DC.B 'Could not create ''%s'' property', 0x0d, 0					; 0x69f6
	DC.B 'AAPL,bootp-client-ip-address', 0								; 0x6a16
	DC.B 'AAPL,bootp-subnet-mask', 0									; 0x6a33
	DC.B 'AAPL,bootp-routers', 0										; 0x6a4a
	DC.B 'AAPL,bootp-gateway-ip-address', 0								; 0x6a5d
	DC.B 'AAPL,bootp-domain-nameservers', 0								; 0x6a7b
	DC.B 0																; 0x6a99
	DC.B 0																; 0x6a9a
	DC.B 0																; 0x6a9b
	DC.B 0																; 0x6a9c
	DC.B 0																; 0x6a9d
	DC.B 0																; 0x6a9e
	DC.B 0																; 0x6a9f
	DC.B 'call-method', 0												; 0x6aa0
	DC.B 'interpret', 0													; 0x6aac
	DC.B 'write', 0														; 0x6ab6
	DC.B 'finddevice', 0												; 0x6abc
	DC.B '/chosen', 0													; 0x6ac7
	DC.B 'mmu', 0														; 0x6acf
	DC.B 'translate', 0													; 0x6ad3
	DC.B 'claim', 0														; 0x6add
	DC.B 'release', 0													; 0x6ae3
	DC.B 'MacOS: ciMapRange -claim- failed.', 0x0d, 0					; 0x6aeb
	DC.B 'map', 0														; 0x6b0e
	DC.B 'MacOS: ciMapRange -map- failed.', 0x0d, 0						; 0x6b12
	DC.B 0																; 0x6b33
	DC.B 0																; 0x6b34
	DC.B 0																; 0x6b35
	DC.B 0																; 0x6b36
	DC.B 0																; 0x6b37
	DC.B '0123456789abcdef', 0											; 0x6b38
	DC.B 0																; 0x6b49
	DC.B 0																; 0x6b4a
	DC.B 0																; 0x6b4b
	DC.B 0																; 0x6b4c
	DC.B 0																; 0x6b4d
	DC.B 0																; 0x6b4e
	DC.B 0																; 0x6b4f
	DC.B '0123456789ABCDEF', 0											; 0x6b50
	DC.B 0																; 0x6b61
	DC.B 0																; 0x6b62
	DC.B 0																; 0x6b63
	DC.B 0																; 0x6b64
	DC.B 0																; 0x6b65
	DC.B 0																; 0x6b66
	DC.B 0																; 0x6b67
