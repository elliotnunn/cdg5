#
#	File:		Parcels.make
#
#	Contains:	Makefile to build "prcl" images for "Mac OS ROM" or Classic's "MacOSROM"
#
#	Change History (most recent first):
#
#				 1/29/17	HQX		Got rid of default rules and ".Prcl" files
#				 1/10/17	HQX		Wrote it.


# Build the image

"{ImageDir}Parcels"			�	{ParcelFiles} "{RsrcDir}Parcels.rsrc" "{ToolDir}ParcelLayout"
	ParcelLayout "{RsrcDir}Parcels.rsrc" {Targ} > "{TextDir}ParcelInfo"


# Parcels.rsrc is to ParcelLayout as RomResources.rsrc is to RomLayout

"{RsrcDir}Parcels.rsrc"		�	"{NewWorldDir}Parcels.r" "{Sources}Internal:Rez:RomTypes.r"

CleanParcels				�
	Delete -i "{ImageDir}Parcels" "{RsrcDir}Parcels.rsrc" � Dev:Null
